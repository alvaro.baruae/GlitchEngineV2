#pragma once
#include <unordered_map>
#include <Math\GEMath.h>
#include<Graphics\Window.h>

#define BEEN_UP -1;
#define BEEN_DOWN -2;

using namespace std;
using namespace GlitchEngine::Graphics;
using namespace GlitchEngine::Math;

namespace GlitchEngine
{
	namespace Utilities
	{
		struct InputState
		{
			int state = BEEN_UP;
			InputState() { }

			InputState& operator=(const int& state)
			{
				this->state = state;
				return *this;
			}

			bool operator==(const int& other)
			{
				return state == other;
			}
		};

		class Input
		{
		public:
			Input();
			
			static Input* GetInstance();
			void Destroy();

			GE_API static bool GetKey(int key);
			GE_API static bool GetKeyUp(int key);
			GE_API static bool GetKeyDown(int key);
			 
			GE_API static Vec2 GetMousePosition();
			GE_API static float GetScrollValue();
			GE_API static bool GetMouseButton(int button);
			GE_API static bool GetMouseButtonUp(int button);
			GE_API static bool GetMouseButtonDown(int button);


		private:

			static Input* Instance;
			static unordered_map<int, InputState> KeysPool;
			static unordered_map<int, InputState> MouseButtonsPool;
			static Vec2 MousePosition;
			static float Scroll;

			static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
			static void MousePositionCallback(GLFWwindow* window, double x, double y);
			static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
			static void MouseScrollCallback(GLFWwindow* window, double x, double y);
		};
	}
}


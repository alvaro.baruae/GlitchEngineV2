#include "Input.h"

namespace GlitchEngine
{
	namespace Utilities
	{
		Input* Input::Instance = nullptr;
		unordered_map<int, InputState> Input::KeysPool;
		unordered_map<int, InputState> Input::MouseButtonsPool;
		Vec2 Input::MousePosition;
		float Input::Scroll;

		Input::Input()
		{
			GLFWwindow* currentWindow = Window::GetInstance()->GetWindow();
			glfwSetKeyCallback(currentWindow, &Input::KeyCallback);
			glfwSetCursorPosCallback(currentWindow, &Input::MousePositionCallback);
			glfwSetMouseButtonCallback(currentWindow, &Input::MouseButtonCallback);
			glfwSetScrollCallback(currentWindow, &Input::MouseScrollCallback);
		}

		Input* Input::GetInstance()
		{
			if (!Instance)
				Instance = new Input();
			return Instance;
		}

		void Input::Destroy()
		{
			if (Instance)
				delete Instance;
		}

		bool Input::GetKey(int key)
		{
			return KeysPool[key] == GLFW_PRESS || KeysPool[key] == GLFW_REPEAT || KeysPool[key] == BEEN_DOWN;
		}

		bool Input::GetKeyUp(int key)
		{
			bool beenUp = KeysPool[key] == GLFW_RELEASE;
			if (beenUp)
				KeysPool[key] = BEEN_UP;
			return beenUp;
		}

		bool Input::GetKeyDown(int key)
		{
			bool beenDown = KeysPool[key] == GLFW_PRESS;
			if (beenDown)
				KeysPool[key] = BEEN_DOWN;
			return beenDown;
		}

		bool Input::GetMouseButton(int button)
		{
			return MouseButtonsPool[button] == BEEN_DOWN;
		}

		bool Input::GetMouseButtonUp(int button)
		{
			bool beenUp = MouseButtonsPool[button] == GLFW_RELEASE;
			if (beenUp)
				MouseButtonsPool[button] = BEEN_UP;
			return beenUp;
		}

		bool Input::GetMouseButtonDown(int button)
		{
			bool beenDown = MouseButtonsPool[button] == GLFW_PRESS;
			if (beenDown)
				MouseButtonsPool[button] = BEEN_DOWN;
			return beenDown;
		}

		Vec2 Input::GetMousePosition()
		{
			return MousePosition;
		}

		float Input::GetScrollValue()
		{
			float realValue = Scroll;
			Scroll = 0;
			return realValue;
		}

		void Input::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
		{
			KeysPool[key] = action;
		}

		void Input::MousePositionCallback(GLFWwindow * window, double x, double y)
		{
			MousePosition.x = (float)x;
			MousePosition.y = (float)y;
		}

		void Input::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
		{
			MouseButtonsPool[button] = action;
		}

		void Input::MouseScrollCallback(GLFWwindow * window, double x, double y)
		{
			Scroll = (float)y;
		}
	}
}

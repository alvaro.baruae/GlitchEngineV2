#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif
#define SHADERS_DIR "Resources/Shaders/"

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include <GL\glew.h>

using namespace std;
namespace GlitchEngine
{
	namespace Utilities
	{
		enum ShaderLocation 
		{
			POSITION,
			NORMAL,
			TEX_COORD,
			SHADERLOCATIONS_COUNT
		};

		using namespace std;

		class ShaderLoader
		{
		private:
			static ShaderLoader* Instance;
			static unordered_map<string, GLuint> Programs;
			string ReadFromFile(string filePath);
			GLuint CompileShader(GLenum mode, const char* source);

		public:
			ShaderLoader();
			~ShaderLoader();
			void Destroy();

			GE_API static ShaderLoader* GetInstance();
			GE_API void LoadShader(string shaderName, string fullFileLocation);
			GE_API void LoadShader(string shaderName, string vert, string frag);
			GE_API static GLuint GetProgram(const char* key);
		};
	}
}
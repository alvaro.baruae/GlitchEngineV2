#include "Time.h"

namespace GlitchEngine
{
	namespace Utilities
	{
		Time* Time::Instance = nullptr;
		
		Time::Time()
		{
		}


		Time::~Time()
		{
		}

		void Time::Destroy()
		{
			delete Instance;
		}
		
		void Time::SetTime(float newTime)
		{
			delta = newTime - time;
			time += delta;
		}

		Time * Time::GetInstance()
		{
			if (!Instance)
				Instance = new Time();
			return Instance;
		}

		float Time::GetDelta()
		{
			return GetInstance()->delta;
		}

		float Time::GetTime()
		{
			return GetInstance()->time;
		}
	}
}
#include "ShaderLoader.h"

namespace GlitchEngine
{
	namespace Utilities
	{
		unordered_map<string, GLuint> ShaderLoader::Programs;
		ShaderLoader* ShaderLoader::Instance = nullptr;

		ShaderLoader::ShaderLoader()
		{
		}

		ShaderLoader::~ShaderLoader()
		{
			for (unordered_map<string, GLuint>::iterator it = Programs.begin(); it != Programs.end(); ++it)
				glDeleteProgram((*it).second);
			Programs.clear();
		}

		ShaderLoader* ShaderLoader::GetInstance()
		{
			if (!Instance)
				Instance = new ShaderLoader();
			return Instance;
		}

		void ShaderLoader::Destroy()
		{
			if (Instance)
				delete Instance;
		}

		string ShaderLoader::ReadFromFile(string filePath)
		{
			FILE *f = fopen(filePath.c_str(), "rt");
			
			if (!f)
			{
				char error[BUFSIZ];
				strerror_s(error, errno);
				cout << "Error while opening '" + filePath + "': " + string(error) << endl;
				return string();
			}

			fseek(f, 0, SEEK_END);

			unsigned long count = ftell(f);
			rewind(f);

			char *data = new char[count + 1];
			count = fread(data, sizeof(char), count, f);
			data[count] = '\0';
			fclose(f);

			string result(data);
			delete[] data;
			return result;
		}

		GLuint ShaderLoader::CompileShader(GLenum mode, const char* source)
		{
			GLuint shaderID = glCreateShader(mode);

			glShaderSource(shaderID, 1, &source, NULL);
			glCompileShader(shaderID);

			GLint result;
			glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
			if (result == GL_FALSE)
			{
				GLint lenght;
				glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &lenght);

				vector<char> error(lenght);
				glGetShaderInfoLog(shaderID, lenght, &lenght, &error[0]);

				if (mode == GL_FRAGMENT_SHADER)
					cout << "Failed to compile fragment shader!" << endl;
				else
					cout << "Failed to compile vertex shader!" << endl;
				cout << &error[0] << endl;
				glDeleteShader(shaderID);
				return 0;
			}

			return shaderID;
		}

		void ShaderLoader::LoadShader(string shaderName, string fullFileLocation)
		{
			string vertexSource = ReadFromFile(fullFileLocation + ".vert");
			string fragmentSource = ReadFromFile(fullFileLocation + ".frag");

			if(!vertexSource.empty() && !fragmentSource.empty())
				LoadShader(shaderName, vertexSource, fragmentSource);
		}

		void ShaderLoader::LoadShader(string shaderName, string vert, string frag)
		{
			GLuint program = glCreateProgram();
			Programs[shaderName] = program;

			GLuint vertex = CompileShader(GL_VERTEX_SHADER, vert.c_str());
			GLuint fragment = CompileShader(GL_FRAGMENT_SHADER, frag.c_str());

			if (vertex != 0 || fragment != 0)
			{
				glAttachShader(program, vertex);
				glAttachShader(program, fragment);

				glBindAttribLocation(program, POSITION, "position");
				glBindAttribLocation(program, NORMAL, "normal");
				glBindAttribLocation(program, TEX_COORD, "texCoord");

				GLint code;
				glLinkProgram(program);
				glGetProgramiv(program, GL_LINK_STATUS, &code);
				if (code == GL_FALSE)
					cout << "Error: Shader '" + shaderName + "' failed to link.";

				glDetachShader(program, vertex);
				glDetachShader(program, fragment);

				glDeleteShader(vertex);
				glDeleteShader(fragment);
			}
		}

		GLuint ShaderLoader::GetProgram(const char* key)
		{
			unordered_map<string, GLuint>::iterator it = Programs.find(key);
			if (it == Programs.end())
				cout << "Shader of name '" << key << "' not found." << endl;
			return it->second;
		}
	}
}
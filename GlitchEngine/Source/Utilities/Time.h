#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif

namespace GlitchEngine
{
	namespace Utilities
	{
		class Time
		{
		public:
			Time();
			~Time();
			void Destroy();

			void SetTime(float newTime);
			static Time* GetInstance();
			GE_API static float GetDelta();
			GE_API static float GetTime();

		private:
			static Time* Instance;

			float delta = 0;
			float time = 0;
		};
	}
}
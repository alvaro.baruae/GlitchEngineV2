#pragma once
#ifdef DDS_LOADER_IMPLEMENTATION

#define FOURCC_DXT1 0x31545844
#define FOURCC_DXT3 0x33545844
#define FOURCC_DXT5 0x35545844
#define DDS_FORMAT_UNSUPPORTED 0x37545844

#include <iostream>
using namespace std;

bool DDS_LOADER_isLittleEndian = false;
inline unsigned int ByteToUInt(unsigned char* bytes, short length)
{
	unsigned int ret = 0;
	if (DDS_LOADER_isLittleEndian)
	{
		for (int i = 0; i < length; ++i)
			((char*)&ret)[i] = bytes[i];
	}
	else
	{
		for (int i = 0; i < length; ++i)
			((char*)&ret)[3 - i] = bytes[i];
	}
	return ret;
}

inline unsigned char* LoadDDS(const char* filePath, int* width, int* height, int* components, unsigned int* format, int* mipMapCount)
{
	int endianTest = 1;
	DDS_LOADER_isLittleEndian = (((char*)&endianTest)[0]);
	FILE* imageFile = fopen(filePath, "rb");

	char magicWord[4];
	unsigned char fileHeader[124];

	if (imageFile)
	{
		fread(magicWord, 1, 4, imageFile);
		if (strncmp(magicWord, "DDS ", 4) != 0)
		{
			cout << "Not a valid DDS file found on: '" << filePath << "'." << endl;
			return NULL;
		}

		fread(&fileHeader, 124, 1, imageFile);
		*height = ByteToUInt(&fileHeader[8], 4);
		*width = ByteToUInt(&fileHeader[12], 4);
		*mipMapCount = ByteToUInt(&fileHeader[24], 4);

		unsigned int linearSize = ByteToUInt(&fileHeader[16], 4);
		unsigned int fourCC = ByteToUInt(&fileHeader[80], 4);

		*components = (fourCC == FOURCC_DXT1) ? 3 : 4;
		switch (fourCC)
		{
		case FOURCC_DXT1:
			*format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
			break;
		case FOURCC_DXT3:
			*format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
			break;
		case FOURCC_DXT5:
			*format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
			break;
		default:
			*format = DDS_FORMAT_UNSUPPORTED;
			return 0;
		}

		unsigned int dataBufferSize = *mipMapCount > 1 ? linearSize * 2 : linearSize;
		unsigned char* imageData = (unsigned char*)malloc(dataBufferSize * sizeof(unsigned char));

		fread(imageData, 1, dataBufferSize, imageFile);
		fclose(imageFile);

		return imageData;
	}
	else
		cout << "Error while opening DDS file: '" << filePath << +"'." << endl;
	return NULL;
}

#endif
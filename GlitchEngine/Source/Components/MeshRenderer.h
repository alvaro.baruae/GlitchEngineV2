#pragma once
#include <Graphics\OGLRenderer.h>
#include <Managers\RendererManager.h>
#include <Components\Component.h>
#include <Components\Camera.h>
#include <Components\Transform.h>
#include <Components\Material.h>
#include <Components\MeshDefinition.h>

using namespace GlitchEngine::Managers;
using namespace GlitchEngine::Graphics;
namespace GlitchEngine
{
	namespace Components
	{
		enum GE_API StencilChannel
		{
			DEFAULT = 1 << 0,
			CHANNEL1 = 1 << 1,
			CHANNEL2 = 1 << 2,
			CHANNEL3 = 1 << 3,
			CHANNEL4 = 1 << 4,
			CHANNEL5 = 1 << 5,
			CHANNEL6 = 1 << 6,
			CHANNEL7 = 1 << 7,
			CHANNEL8 = 1 << 8
		};

		class GE_API MeshRenderer : public Component
		{
		public:
			MeshRenderer() { };
			virtual ~MeshRenderer() override;

			virtual void Init() override;
			void Draw();
			void InitRenderer();
			void EnableStencilTest(int stencilBit);
			void EnableStencilTest(int stencilBit, GLuint mask, GLuint function, GLuint sFail, GLuint dFail, GLuint dPass);
			void DisableStencilTest();

		private:
			CompPtr<MeshDefinition> meshDefinition;
			CompPtr<Transform> transform;
			CompPtr<Material> material;
			OGLRenderer* renderer;
		};
	}
}
#include "Camera.h"
#include <Entities\Entity.h>

namespace GlitchEngine
{
	namespace Components
	{
		Camera* Camera::ActiveCamera = nullptr;

		void Camera::Init()
		{
			gameWindow = Window::GetInstance();
			transform = entity->GetComponent<Transform>();
			SetAsPerspective(45.0f, gameWindow->GetAspect(), 1.0f, 10000.0f);
		}

		void Camera::Update()
		{
			view = transform->GetWorldMatrix();
		}

		void Camera::SetAsOrtographic()
		{
			float halfWidth = gameWindow->GetWidth() / 2.f;
			float halfHeight = gameWindow->GetHeight() / 2.f;
			projection = Mat4::Ortographic(-halfWidth, halfWidth, halfHeight, -halfHeight, -1.0f, 10000.0f);
		}

		void Camera::SetAsPerspective(float fov, float aspect, float near, float far)
		{
			projection = Mat4::Perspective(fov, aspect, near, far);
		}

		Mat4 Camera::GetProjectionMatrix() const
		{
			return projection;
		}

		Mat4 Camera::GetViewMatrix() const
		{
			return view;
		}

		void Camera::SetAsActiveCamera()
		{
			ActiveCamera = this;
		}
	}
}
#pragma once
#include <vector>
#include <Math\GEMath.h>
#include <Components\Component.h>

using namespace std;
using namespace GlitchEngine::Math;
using namespace GlitchEngine::Components;

namespace GlitchEngine
{
	namespace Components
	{
		enum GE_API Space
		{
			WorldSpace = 0,
			LocalSpace = 1
		};

		class GE_API Transform : public Component
		{
		public:
			Transform();

			virtual void Init() override;
			virtual void Update() override;

			Vec3 GetLocalPosition() const;
			Vec3 GetLocalRotation() const;
			Vec3 GetScale() const;

			void SetPosition(Vec3 newPos);
			void SetRotation(float angle, Vec3 axis);
			void SetScale(Vec3 scale);

			void Translate(Vec3 trans, unsigned short space);
			void Scale(Vec3 scale);
			void Rotate(float angle, Vec3 axis);
			void Rotate(float angle);

			Mat4 GetLocalMatrix() const;
			Mat4 GetWorldMatrix() const;
			Mat4 GetModelMatrix() const;

			Mat4 GetTranslationMatrix() const;
			Mat4 GetRotationMatrix() const;
			Mat4 GetScaleMatrix() const;

			void ForceUpdate();
			void AddChild(CompPtr<Transform> child);
			void RemoveChild(unsigned int index);
			CompPtr<Transform> GetChild(unsigned int index);
			void SetParent(CompPtr<Transform> p);
			bool HaveParent() const;

		private:
			bool haveToUpdate = false;
			CompPtr<Transform> parent;
			vector<CompPtr<Transform>> children;

			Mat4 translationMatrix;
			Mat4 rotationMatrix;
			Mat4 scaleMatrix;
			Mat4 localMatrix;
			Mat4 worldMatrix;

			Vec3 localPosition;
			Vec3 localScale;
			Vec3 localRotation;

			void DecomposeWorldMatrix();
		};
	}
}
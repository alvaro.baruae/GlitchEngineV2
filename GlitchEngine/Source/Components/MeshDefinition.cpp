#include "MeshDefinition.h"

namespace GlitchEngine
{
	namespace Components
	{
		void MeshDefinition::Init()
		{
			resourcesManager = ResourcesManager::GetInstance();

			isActive = false;
			canModifyActiveState = false;
		}

		void MeshDefinition::SetAsCube()
		{
			isPrimitive = true;
			primitiveType = GL_TRIANGLES;

			vertices = {
				Vec3(-1.0f, -1.0f, -1.0f),
				Vec3(-1.0f, -1.0f,  1.0f),
				Vec3(-1.0f,  1.0f,  1.0f),
				Vec3( 1.0f,  1.0f, -1.0f),
				Vec3(-1.0f, -1.0f, -1.0f),
				Vec3(-1.0f,  1.0f, -1.0f),
				Vec3( 1.0f, -1.0f,  1.0f),
				Vec3(-1.0f, -1.0f, -1.0f),
				Vec3( 1.0f, -1.0f, -1.0f),
				Vec3( 1.0f,  1.0f, -1.0f),
				Vec3( 1.0f, -1.0f, -1.0f),
				Vec3(-1.0f, -1.0f, -1.0f),
				Vec3(-1.0f, -1.0f, -1.0f),
				Vec3(-1.0f,  1.0f,  1.0f),
				Vec3(-1.0f,  1.0f, -1.0f),
				Vec3( 1.0f, -1.0f,  1.0f),
				Vec3(-1.0f, -1.0f,  1.0f),
				Vec3(-1.0f, -1.0f, -1.0f),
				Vec3(-1.0f,  1.0f,  1.0f),
				Vec3(-1.0f, -1.0f,  1.0f),
				Vec3( 1.0f, -1.0f,  1.0f),
				Vec3( 1.0f,  1.0f,  1.0f),
				Vec3( 1.0f, -1.0f, -1.0f),
				Vec3( 1.0f,  1.0f, -1.0f),
				Vec3( 1.0f, -1.0f, -1.0f),
				Vec3( 1.0f,  1.0f,  1.0f),
				Vec3( 1.0f, -1.0f,  1.0f),
				Vec3( 1.0f,  1.0f,  1.0f),
				Vec3( 1.0f,  1.0f, -1.0f),
				Vec3(-1.0f,  1.0f, -1.0f),
				Vec3( 1.0f,  1.0f,  1.0f),
				Vec3(-1.0f,  1.0f, -1.0f),
				Vec3(-1.0f,  1.0f,  1.0f),
				Vec3( 1.0f,  1.0f,  1.0f),
				Vec3(-1.0f,  1.0f,  1.0f),
				Vec3( 1.0f, -1.0f,  1.0f)
			};

			meshLoaded = true;
		}

		void MeshDefinition::SetAsTriangle()
		{
			isPrimitive = true;
			primitiveType = GL_TRIANGLES;

			vertices = {
				Vec3( 0.0f,  0.5f, 0.0f),
				Vec3( 0.5f, -0.5f, 0.0f),
				Vec3(-0.5f, -0.5f, 0.0f)
			};

			texCoords = {
				Vec2(0.5f, 0.0f),
				Vec2(1.0f, 1.0f),
				Vec2(0.0f, 1.0f)
			};

			meshLoaded = true;
		}

		void MeshDefinition::LoadMesh(string key)
		{
			Model* model = resourcesManager->GetModel(key);
			loadedMesh = model->GetMeshTree()[0];
			
			isPrimitive = false;
			primitiveType = GL_TRIANGLES;
			meshLoaded = true;

			//GET MODEL OR MESH FROM RESOURCES
			//Iterate model
			//Chech if more entities need to be created for model and do so.
			//set loaded mesh to root
		}
	}
}
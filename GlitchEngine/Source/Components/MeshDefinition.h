#pragma once
#include <vector>
#include <GL\glew.h>
#include <Math\GEMath.h>
#include <Components\Component.h>
#include <Managers\ResourcesManager.h>
#include <Graphics\Mesh.h>
#include <Graphics\Model.h>

using namespace std;
using namespace GlitchEngine::Graphics;
using namespace GlitchEngine::Managers;

namespace GlitchEngine
{
	using namespace Math;
	namespace Components
	{
		class MeshDefinition : public Component
		{
		public:
			MeshDefinition() { }
			virtual void Init() override;
			
			GE_API void SetAsCube();
			GE_API void SetAsTriangle();
			
			GE_API void LoadMesh(string key);
			GE_API vector<Vec3> GetVertices() const { return vertices; }
			GE_API vector<Vec3> GetNormals() const { return normals; }
			GE_API vector<Vec2> GetTextureCoords() const { return texCoords; }
			
			GE_API void SetVertices(vector<Vec3> verts) { vertices = verts; }
			GE_API void SetNormals(vector<Vec3> norms) { normals = norms; }
			GE_API void SetTextureCoords(vector<Vec2> coords) { texCoords = coords; }
			
			GE_API bool IsPrimitive() const { return isPrimitive; }
			GE_API bool MeshLoaded() const { return meshLoaded; }
			GLuint GetPrimitiveType() const { return primitiveType; }
			Mesh* GetMesh() const { return loadedMesh; }

		private:
			ResourcesManager* resourcesManager;
			vector<Vec3> vertices;
			vector<Vec3> normals;
			vector<Vec2> texCoords;
			GLuint primitiveType;

			Mesh* loadedMesh;
			bool meshLoaded = false;
			bool isPrimitive = false;
		};
	}
}

#pragma once
#include <Math\GEMath.h>
#include <Graphics\Window.h>
#include <Components\Component.h>
#include <Components\Transform.h>

using namespace GlitchEngine::Graphics;
using namespace GlitchEngine::Math;

namespace GlitchEngine 
{
	namespace Components
	{
		class GE_API Camera : public Component
		{
		public:
			static Camera* ActiveCamera;

			Camera() { };

			virtual void Init() override;
			virtual void Update() override;

			void SetAsOrtographic();
			void SetAsPerspective(float fov, float aspect, float near, float far);

			Mat4 GetProjectionMatrix() const;
			Mat4 GetViewMatrix() const;

			void SetAsActiveCamera();
		private:
			CompPtr<Transform> transform;
			Window* gameWindow;
			Mat4 projection;
			Mat4 view;
		};
	}
}

#include "Transform.h"
#include <Entities\Entity.h>

namespace GlitchEngine
{
	namespace Components
	{
		Transform::Transform() { }

		void Transform::Init()
		{
			canModifyActiveState = false;
			
			translationMatrix = Mat4::Identity();
			scaleMatrix = Mat4::Identity();
			rotationMatrix = Mat4::Identity();

			worldMatrix = Mat4::Identity();
			localMatrix = Mat4::Identity();
			
			localScale = Vec3(1, 1, 1);
		}

		void Transform::Update()
		{
			if (haveToUpdate)
			{
				if (parent)
					worldMatrix = parent->GetWorldMatrix() * localMatrix;
				else
					worldMatrix = localMatrix;

				for (auto& it : children)
					it->ForceUpdate();
				haveToUpdate = false;
			}
		}

		Vec3 Transform::GetLocalPosition() const
		{
			return localPosition;
		}

		Vec3 Transform::GetLocalRotation() const
		{
			return localRotation;
		}

		Vec3 Transform::GetScale() const
		{
			return localScale;
		}

		void Transform::SetPosition(Vec3 newPos)
		{
			localPosition = newPos;
			translationMatrix[3][0] = localPosition.x;
			translationMatrix[3][1] = localPosition.y;
			translationMatrix[3][2] = localPosition.z;

			localMatrix = translationMatrix * rotationMatrix;
			haveToUpdate = true;
		}

		void Transform::SetRotation(float angle, Vec3 axis)
		{
			localRotation = axis * angle;
			rotationMatrix = Mat4::Rotation(angle, axis);

			localMatrix = translationMatrix * rotationMatrix;
			haveToUpdate = true;
		}

		void Transform::SetScale(Vec3 scale)
		{
			localScale = scale;
			scaleMatrix[0][0] = localScale.x;
			scaleMatrix[1][1] = localScale.y;
			scaleMatrix[2][2] = localScale.z;
		}

		void Transform::Translate(Vec3 trans, unsigned short space)
		{
			if (space == Space::LocalSpace)
				trans = rotationMatrix * trans;

			localPosition += trans;
			translationMatrix[3][0] = localPosition.x;
			translationMatrix[3][1] = localPosition.y;
			translationMatrix[3][2] = localPosition.z;

			localMatrix = translationMatrix * rotationMatrix;
			haveToUpdate = true;
		}

		void Transform::Scale(Vec3 scale)
		{
			localScale *= scale;
			scaleMatrix[0][0] = localScale.x;
			scaleMatrix[1][1] = localScale.y;
			scaleMatrix[2][2] = localScale.z;
		}

		void Transform::Rotate(float angle, Vec3 axis)
		{
			localRotation += axis * angle;
			rotationMatrix.Rotate(angle, axis);

			localMatrix = translationMatrix * rotationMatrix;
			haveToUpdate = true;
		}

		void Transform::Rotate(float angle)
		{
			Rotate(angle, Vec3::AxisZ);
		}

		Mat4 Transform::GetLocalMatrix() const
		{
			return translationMatrix * rotationMatrix * scaleMatrix;
		}

		Mat4 Transform::GetWorldMatrix() const
		{
			return worldMatrix;
		}

		Mat4 Transform::GetModelMatrix() const
		{
			return worldMatrix * scaleMatrix;
		}

		Mat4 Transform::GetTranslationMatrix() const
		{
			return translationMatrix;
		}

		Mat4 Transform::GetRotationMatrix() const
		{
			return rotationMatrix;
		}

		Mat4 Transform::GetScaleMatrix() const
		{
			return scaleMatrix;
		}

		void Transform::ForceUpdate()
		{
			haveToUpdate = true;
		}

		void Transform::AddChild(CompPtr<Transform> child)
		{
			if (!child->HaveParent())
			{
				child->SetParent(this);
				children.push_back(child);
			}
			else
				cout << "The transform you are trying to add as a child already have a parent." << endl;
		}

		void Transform::RemoveChild(unsigned int index)
		{
			if (index < children.size())
			{
				vector<CompPtr<Transform>>::iterator it = children.begin() + index;
				(*it)->SetParent(nullptr);
				children.erase(children.begin() + index);
			}
			else
				cout << "Children not found at index '" << index << "' on entity '" << entity->GetName() << "'." << endl;
		}

		CompPtr<Transform> Transform::GetChild(unsigned int index)
		{
			CompPtr<Transform> ret;
			try
			{
				ret = children.at(index);
			}
			catch (...)
			{
				cout << "Children not found at index '" << index << "' on entity '" << entity->GetName() << "'." << endl;
			}
			return ret;
		}

		void Transform::SetParent(CompPtr<Transform> p)
		{
			parent = p;
			if (!p)
			{
				localMatrix = worldMatrix;
				DecomposeWorldMatrix();
			}
			else
			{
				Mat4 parentWM = parent->GetWorldMatrix();
				localMatrix = parentWM.Inverse() * localMatrix;
			}

			ForceUpdate();
		}

		bool Transform::HaveParent() const
		{
			return parent;
		}

		void Transform::DecomposeWorldMatrix()
		{
			localPosition = Vec3(worldMatrix[3][0], worldMatrix[3][1], worldMatrix[3][2]);
			localRotation.x = atan2(worldMatrix[2][1], worldMatrix[2][2]);

			float c2 = sqrt(pow(worldMatrix[0][0], 2) + pow(worldMatrix[1][0], 2));
			localRotation.y = atan2(-worldMatrix[2][0], c2);

			float s1 = sin(localRotation.x);
			float c1 = cos(localRotation.x);

			localRotation.z = atan2(s1*worldMatrix[0][2] - c1 * worldMatrix[0][1], c1 * worldMatrix[1][1] - s1 * worldMatrix[1][2]);
		}
	}
}
#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include <Math\GEMath.h>
#include <Components\Component.h>
#include <Graphics\OGLRenderer.h>
#include <Utilities\ShaderLoader.h>
#include <Managers\ResourcesManager.h>

using namespace std;
using namespace GlitchEngine::Graphics;
using namespace GlitchEngine::Math;
using namespace GlitchEngine::Utilities;
using namespace GlitchEngine::Managers;

namespace GlitchEngine
{
	namespace Components
	{
		class Material : public Component
		{
		public:
			Material() { };
			virtual void Init() override;

			GE_API void SetShader(string shader);
			GE_API void SetShaderUniforms(OGLRenderer* renderer);
			GE_API void SetShaderTexture(string name, string key);
			GE_API void SetShaderProperty(string name, int& value);
			GE_API void SetShaderProperty(string name, float& value);
			GE_API void SetShaderProperty(string name, Vec2& value);
			GE_API void SetShaderProperty(string name, Vec3& value);
			GE_API void SetShaderProperty(string name, Vec4& value);
			GE_API void SetShaderProperty(string name, Mat3& value);
			GE_API void SetShaderProperty(string name, Mat4& value);
			
			GLuint GetProgram() const;
			bool ShaderSet() const;
		
		private:
			GLuint program;
			bool shaderSet = false;
			bool textureChanged = false;
			ResourcesManager* resources;

			unordered_map<string, Texture2D*> textures;
			unordered_map<string, int> intProperties;
			unordered_map<string, float> floatProperties;
			unordered_map<string, Vec2> vec2Properties;
			unordered_map<string, Vec3> vec3Properties;
			unordered_map<string, Vec4> vec4Properties;
			unordered_map<string, Mat3> mat3Properties;
			unordered_map<string, Mat4> mat4Properties;
		};
	}
}
#pragma once
#pragma warning(disable:4251)
#define GE_API_EXPORT __declspec(dllexport)

#include <iostream>
using namespace std;

namespace GlitchEngine
{
	namespace Entities { class Entity; }
	using namespace Entities;

	namespace Components
	{
		inline unsigned short GetComponentID()
		{
			static unsigned short lastID = 0;
			return lastID++;
		}

		template <typename T> 
		inline unsigned short GetComponentID()
		{
			static unsigned short typeID = GetComponentID();
			return typeID;
		}

		class GE_API_EXPORT Component
		{
		protected:
			bool isActive = true;
			bool canModifyActiveState = true;
		
		public:
			Entity* entity;
			virtual ~Component() { }
			virtual void Init() { };
			virtual void Update() { };
			virtual void Enable() { };
			virtual void Disable() { };
			virtual void Remove() { };

			bool SetActiveState(bool newActiveState) { if (canModifyActiveState) { isActive = newActiveState; return true; } return false; }
			bool GetActiveState() const { return isActive; }
		};

#ifdef _DEBUG
		struct CompBase
		{
			Component* component;
			bool* isDead = nullptr;
		};

		template <typename T>
		struct GE_API_EXPORT CompPtr : CompBase
		{
			CompPtr() { }
			CompPtr(T* comp) 
			{
				component = comp;
				isDead = new bool(false); 
			}

			T* operator->()
			{
				if (!isDead || (*isDead) == true)
				{
					cout << "Component of type '"<< typeid(T).name() << "' has already been removed or the entity was destroyed and you are still trying to access it." << endl;
					return nullptr;
				}
				return static_cast<T*>(component);
			}

			operator bool() const
			{
				return component != nullptr;
			}
		};		
#else
		template <typename T>
		using CompPtr = T*;
#endif
	}
}
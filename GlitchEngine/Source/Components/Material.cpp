#include "Material.h"

namespace GlitchEngine
{
	namespace Components
	{
		void Material::Init()
		{
			isActive = false;
			canModifyActiveState = false;
			resources = ResourcesManager::GetInstance();
		}

		void Material::SetShader(string shader)
		{
			program = ShaderLoader::GetProgram(shader.c_str());
			shaderSet = true;
		}

		void Material::SetShaderUniforms(OGLRenderer* renderer)
		{
			if (textureChanged)
			{
				for (auto const& it : textures)
					renderer->SetTexture(it.first, *it.second);
				textureChanged = false;
			}

			for (auto const& it : intProperties)
				renderer->SetUniform(it.first, it.second);
			for (auto const& it : floatProperties)
				renderer->SetUniform(it.first, it.second);
			for (auto const& it : vec2Properties)
				renderer->SetUniform(it.first, it.second);
			for (auto const& it : vec3Properties)
				renderer->SetUniform(it.first, it.second);
			for (auto const& it : vec4Properties)
				renderer->SetUniform(it.first, it.second);
			for (auto const& it : mat3Properties)
				renderer->SetUniform(it.first, it.second);
			for (auto const& it : mat4Properties)
				renderer->SetUniform(it.first, it.second);
		}

		void Material::SetShaderTexture(string name, string key)
		{
			textures[name] = resources->GetTexture(key);
			textureChanged = true;
		}

		void Material::SetShaderProperty(string name, int & value)
		{
			intProperties[name] = value;
		}

		void Material::SetShaderProperty(string name, float & value)
		{
			floatProperties[name] = value;
		}

		void Material::SetShaderProperty(string name, Vec2 & value)
		{
			vec2Properties[name] = value;
		}

		void Material::SetShaderProperty(string name, Vec3 & value)
		{
			vec3Properties[name] = value;
		}

		void Material::SetShaderProperty(string name, Vec4 & value)
		{
			vec4Properties[name] = value;
		}

		void Material::SetShaderProperty(string name, Mat3 & value)
		{
			mat3Properties[name] = value;
		}

		void Material::SetShaderProperty(string name, Mat4 & value)
		{
			mat4Properties[name] = value;
		}

		GLuint Material::GetProgram() const
		{
			return program;
		}

		bool Material::ShaderSet() const
		{
			return shaderSet;
		}
	}
}
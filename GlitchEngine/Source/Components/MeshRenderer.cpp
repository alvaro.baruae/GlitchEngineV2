#include "MeshRenderer.h"
#include <Entities/Entity.h>

namespace GlitchEngine
{
	namespace Components
	{
		MeshRenderer::~MeshRenderer()
		{
			if (renderer)
				delete renderer;
		}

		void MeshRenderer::Init()
		{
			transform = entity->GetComponent<Transform>();
			meshDefinition = entity->GetComponent<MeshDefinition>();
			material = entity->GetComponent<Material>();
			InitRenderer();
		}

		void MeshRenderer::Draw()
		{
			if (isActive)
			{
				renderer->SetMVP(Camera::ActiveCamera->GetProjectionMatrix() * Camera::ActiveCamera->GetViewMatrix() * transform->GetModelMatrix());
				material->SetShaderUniforms(renderer);
				renderer->Draw();
			}
		}

		void MeshRenderer::InitRenderer()
		{
			if (meshDefinition->MeshLoaded() && material->ShaderSet())
			{
				renderer = new OGLRenderer(material->GetProgram(), meshDefinition->GetPrimitiveType());
				if (meshDefinition->IsPrimitive())
				{
					renderer->UploadVertices(meshDefinition->GetVertices());
					renderer->UploadNormals(meshDefinition->GetNormals());
					renderer->UploadTexCoords(meshDefinition->GetTextureCoords());
				}
				else
					renderer->SetVertexDataFromMesh(meshDefinition->GetMesh());
				RendererManager::GetInstance()->AddRenderer(this);
			}
		}

		void MeshRenderer::EnableStencilTest(int stencilBit)
		{
			static_cast<OGLRenderer*>(renderer)->SetStencilOptions(true, stencilBit);
		}

		void MeshRenderer::EnableStencilTest(int stencilBit, GLuint mask, GLuint function, GLuint sFail, GLuint dFail, GLuint dPass)
		{
			static_cast<OGLRenderer*>(renderer)->SetStencilOptions(true, stencilBit, mask, function, sFail, dFail, dPass);
		}

		void MeshRenderer::DisableStencilTest()
		{
			static_cast<OGLRenderer*>(renderer)->SetStencilOptions(false, 0);
		}
	}
}
#include "Model.h"

namespace GlitchEngine
{
	namespace Graphics
	{
		Model::Model(string& filePath, bool flipUVs)
		{
			Assimp::Importer importer;
			unsigned int flags = aiProcess_Triangulate | aiProcess_CalcTangentSpace | aiProcess_JoinIdenticalVertices | aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph;
			if (flipUVs)
				flags |= aiProcess_FlipUVs;

			const aiScene* scene = importer.ReadFile(filePath, flags);

			if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
			{
				cout << "Error loading model at '" << filePath << "': " << importer.GetErrorString() << endl;
				return;
			}

			RecursiveMeshLookUp(scene->mRootNode, scene);
		}

		Model::~Model()
		{
			for (auto const& it : meshes)
				delete it;
		}

		void Model::RecursiveMeshLookUp(aiNode* node, const aiScene* scene)
		{
			for (size_t i = 0; i < node->mNumMeshes; i++)
				meshes.push_back(ProcessMesh(scene->mMeshes[node->mMeshes[i]], scene));

			for (size_t i = 0; i < node->mNumChildren; i++)
				RecursiveMeshLookUp(node->mChildren[i], scene);
		}

		Mesh* Model::ProcessMesh(aiMesh* aiMesh, const aiScene *scene)
		{
			vector<Vertex> vertices;
			vector<unsigned int> indices;

			for (size_t i = 0; i < aiMesh->mNumVertices; ++i)
			{
				Vertex vertex;

				Vec3 vector;
				vector.x = aiMesh->mVertices[i].x;
				vector.y = aiMesh->mVertices[i].y;
				vector.z = aiMesh->mVertices[i].z;
				vertex.position = vector;
				
				vector.x = aiMesh->mNormals[i].x;
				vector.y = aiMesh->mNormals[i].y;
				vector.z = aiMesh->mNormals[i].z;
				vertex.normal = vector;
				
				if (aiMesh->mTextureCoords[0])
				{
					Vec2 vec;
					vec.x = aiMesh->mTextureCoords[0][i].x;
					vec.y = aiMesh->mTextureCoords[0][i].y;
					vertex.texCoord = vec;
				}
				else
					vertex.texCoord = Vec2(0.0f, 0.0f);
				

				vector.x = aiMesh->mTangents[i].x;
				vector.y = aiMesh->mTangents[i].y;
				vector.z = aiMesh->mTangents[i].z;
				vertex.tangent = vector;
				
				vector.x = aiMesh->mBitangents[i].x;
				vector.y = aiMesh->mBitangents[i].y;
				vector.z = aiMesh->mBitangents[i].z;
				vertex.biTangent = vector;
				vertices.push_back(vertex);
			}
			
			for (size_t i = 0; i < aiMesh->mNumFaces; ++i)
			{
				aiFace face = aiMesh->mFaces[i];
				for (size_t j = 0; j < face.mNumIndices; ++j)
					indices.push_back(face.mIndices[j]);
			}

			return new Mesh(vertices, indices);
		}
	}
}
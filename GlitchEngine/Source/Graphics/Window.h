#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif

#include <iostream>
#include <GL\glew.h>
#include <GLFW\glfw3.h>

namespace GlitchEngine
{
	namespace Graphics
	{
		class Window
		{
		private:
			static Window* Instance;

			GLFWwindow* window;
			GLFWmonitor* monitor;
			const GLFWvidmode* videoMode;

			const char* windowName;
			int width, height;
			float aspect;

			bool vsync = true;
			float targetFPS = 1.f / 60.f;
			float timedUpdateCounter = 0.f;
			float timedUpdateLastTime = 0.f;

			static void WindowResizeCallback(GLFWwindow * window, int width, int height) { GetInstance()->WindowResizeCallbackImpl(window, width, height); }
			void WindowResizeCallbackImpl(GLFWwindow * window, int width, int height);

		public:
			Window();
			~Window();

			GE_API static Window* GetInstance();
			void Destroy() const;

			bool InitWindow(const char* windowName, int width, int height);

			void Update() const;
			bool Closed() const;
			void Clear() const;
			GE_API void SetClearColor(float r, float g, float b) const;
			GE_API void SetResolution(int width, int  height);
			GE_API void SetFullScreen(bool fullScreen);

			bool IsVsyncEnabled() const;
			bool WaitForTargetFPS(float time);

			GE_API int GetWidth() const;
			GE_API int GetHeight() const;
			GE_API float GetAspect() const;
			GLFWwindow* GetWindow();
		};
	}
}
#pragma once
#include <Math\GEMath.h>
#include <Utilities\ShaderLoader.h>
#include <Graphics\Texture2D.h>
#include <Graphics\Mesh.h>
#include <vector>
#include <unordered_map>

using namespace std;
using namespace GlitchEngine::Math;
using namespace GlitchEngine::Utilities;
using namespace GlitchEngine::Graphics;

namespace GlitchEngine
{
	namespace Graphics
	{
		class OGLRenderer
		{
		public:
			OGLRenderer(GLuint program, GLuint primType);
			~OGLRenderer();

			void Draw();

			void SetVertexDataFromMesh(const Mesh* mesh);

			void UploadVertices(const vector<Vec3>& vertices);
			void UploadNormals(const vector<Vec3>& normals);
			void UploadTexCoords(const vector<Vec2>& texCoords);

			void SetMVP(const Mat4& model);

			void SetTexture(const string uniform, const Texture2D& texture);
			void SetUniform(const string uniform, const int& value);
			void SetUniform(const string uniform, const float& value);
			void SetUniform(const string uniform, const Vec2& value);
			void SetUniform(const string uniform, const Vec3& value);
			void SetUniform(const string uniform, const Vec4& value);
			void SetUniform(const string uniform, const Mat3& value);
			void SetUniform(const string uniform, const Mat4& value);
			
			void SetStencilOptions(bool use, GLuint bit);
			void SetStencilOptions(bool use, GLuint bit, GLuint mask, GLuint compare, GLuint sfail, GLuint dfail, GLuint dpass);
			
		private:
			bool useIndices = false;
			GLuint indicesCount = 0;
			GLuint EBO = 0;

			bool stencilTest = false;
			bool writeToStencil = false;
			GLuint stencilBit = 0;
			GLuint stencilMask = 0;
			GLuint stencilCompareFn = 0;
			GLuint sFail = 0;
			GLuint dFail = 0;
			GLuint dPass = 0;

			struct TexturePair
			{
				GLint uniformLocation;
				GLuint textureID;
			};

			GLint mvpLocation;
			GLuint verticesCount;
			GLuint primitiveType;
			GLuint vertexArray;
			GLuint program;
			GLuint matricesBlockIndex;
			GLuint vertexbuffers[SHADERLOCATIONS_COUNT];
			unordered_map<string, TexturePair> textures;
			unordered_map<string, GLint> uniformLocations;
		};
	}
}
#include "Window.h"
namespace GlitchEngine
{
	namespace Graphics
	{
		Window* Window::Instance = nullptr;

		Window::Window()
		{
		}

		Window::~Window()
		{
			glfwDestroyWindow(window);
			glfwTerminate();
		}

		Window* Window::GetInstance()
		{
			if (!Instance)
				Instance = new Window();
			return Instance;
		}

		void Window::Destroy() const
		{
			if (Instance)
				delete Instance;
		}

		bool Window::InitWindow(const char* windowName, int width, int height)
		{
			this->windowName = windowName;
			this->width = width;
			this->height = height;

			if (!glfwInit())
			{
				printf("Failed to initialize GLFW3! \n");
				return false;
			}

			//For resizing windowed windows
			glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

			//For fullscreen add 'monitor' on the 4th parameter
			monitor = glfwGetPrimaryMonitor();
			videoMode = glfwGetVideoMode(monitor);

			//this->width = mode->width;
			//this->height = mode->height;

			glfwWindowHint(GLFW_RED_BITS, videoMode->redBits);
			glfwWindowHint(GLFW_GREEN_BITS, videoMode->greenBits);
			glfwWindowHint(GLFW_BLUE_BITS, videoMode->blueBits);
			glfwWindowHint(GLFW_REFRESH_RATE, videoMode->refreshRate);

			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

			window = glfwCreateWindow(width, height, windowName, NULL, NULL);

			if (!window)
			{
				glfwTerminate();
				printf("Failed to create GLFW Window!");
				return false;
			}

			//For mantaining aspect ratio on resize
			aspect = 16.f / 9.f;
			glfwSetWindowAspectRatio(window, 16, 9);

			glfwMakeContextCurrent(window);
			glfwSetWindowSizeCallback(window, &Window::WindowResizeCallback);

			//For VSYNC
			glfwSwapInterval(1);
			glViewport(0, 0, width, height);

			if (glewInit() != GLEW_OK)
			{
				glfwTerminate();
				glfwDestroyWindow(window);
				printf("Failed to initialize GLEW");
				return false;
			}

			glEnable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);
			glClearStencil(GL_ONE);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			return true;
		}

		void Window::Update() const
		{
			glfwPollEvents();
			glfwSwapBuffers(window);
		}

		bool Window::Closed() const
		{
			return glfwWindowShouldClose(window);
		}

		void Window::Clear() const
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		}

		void Window::WindowResizeCallbackImpl(GLFWwindow * window, int width, int height)
		{
			this->width = width;
			this->height = height;
			glViewport(0, 0, width, height);
		}

		void Window::SetClearColor(float r, float g, float b) const
		{
			glClearColor(r, g, b, 1.f);
		}

		void Window::SetResolution(int width, int height)
		{
			this->width = width;
			this->height = height;
			glfwSetWindowSize(window, width, height);
		}

		void Window::SetFullScreen(bool fullScreen)
		{
			if (fullScreen)
				glfwSetWindowMonitor(window, monitor, 0, 0, width, height, videoMode->refreshRate);
			else
				glfwSetWindowMonitor(window, NULL, 0, 0, width, height, videoMode->refreshRate);
		}

		bool Window::IsVsyncEnabled() const
		{
			return vsync;
		}

		bool Window::WaitForTargetFPS(float time)
		{
			float delta = time - timedUpdateLastTime;
			timedUpdateCounter += delta;
			timedUpdateLastTime = time;
			if (timedUpdateCounter >= targetFPS)
			{
				timedUpdateCounter -= targetFPS;
				return true;
			}
			return false;
		}

		int Window::GetWidth() const
		{
			return width;
		}

		int Window::GetHeight() const
		{
			return height;
		}

		float Window::GetAspect() const
		{
			return aspect;
		}

		GLFWwindow * Window::GetWindow()
		{
			return window;
		}
	}
}
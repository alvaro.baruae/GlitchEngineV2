#include "Mesh.h"

namespace GlitchEngine
{
	namespace Graphics
	{
		Mesh::Mesh(vector<Vertex>& vertices, vector<GLuint>& indices)
		{
			glGenBuffers(2, bufferObjects);

			glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[VERTEX_BUFFER]);
			glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

			indicesCount = indices.size();
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[ELEMENT_BUFFER]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
		}

		Mesh::~Mesh()
		{
			glDeleteBuffers(2, bufferObjects);
		}

		GLuint Mesh::GetVBO() const
		{
			return bufferObjects[VERTEX_BUFFER];
		}

		GLuint Mesh::GetEBO() const
		{
			return bufferObjects[ELEMENT_BUFFER];
		}

		GLuint Mesh::GetIndicesCount() const
		{
			return indicesCount;
		}
	}
}
#include "OGLRenderer.h"

namespace GlitchEngine
{
	namespace Graphics
	{
		OGLRenderer::OGLRenderer(GLuint prog, GLuint primType)
		{
			glGenVertexArrays(1, &vertexArray);
			primitiveType = primType;

			program = prog;
			mvpLocation = glGetUniformLocation(program, "mvp");
		}

		OGLRenderer::~OGLRenderer()
		{
			glDeleteVertexArrays(1, &vertexArray);
			glDeleteBuffers(SHADERLOCATIONS_COUNT, vertexbuffers);
		}

		void OGLRenderer::Draw()
		{
			if (stencilTest)
			{
				glEnable(GL_STENCIL_TEST);
				if (writeToStencil)
				{
					glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
					glStencilFunc(GL_ALWAYS, stencilBit, 0xFF);
					glStencilOp(GL_ZERO, GL_KEEP, GL_REPLACE);
				}
				else
				{
					glStencilFunc(stencilCompareFn, stencilBit, stencilMask);
					glStencilOp(sFail, dFail, dPass);
				}
			}


			GLuint unitIndex = 0;
			for (unordered_map<string, TexturePair>::iterator it = textures.begin(); it != textures.end(); ++it)
			{
				glActiveTexture(GL_TEXTURE0 + unitIndex);
				glBindTexture(GL_TEXTURE_2D, it->second.textureID);
				glUniform1i(it->second.uniformLocation, unitIndex);
				++unitIndex;
			}

			glBindVertexArray(vertexArray);

			if(useIndices)
				glDrawElements(primitiveType, indicesCount, GL_UNSIGNED_INT, 0);
			else
				glDrawArrays(primitiveType, 0, verticesCount);

			glBindVertexArray(0);
			glUseProgram(0);
			
			if (stencilTest)
			{
				glDisable(GL_STENCIL_TEST);
				if (writeToStencil)
					glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			}
		}

		void OGLRenderer::SetVertexDataFromMesh(const Mesh* mesh)
		{
			useIndices = true;
			indicesCount = mesh->GetIndicesCount();

			glBindVertexArray(vertexArray);
			glBindBuffer(GL_ARRAY_BUFFER, mesh->GetVBO());

			glEnableVertexAttribArray(POSITION);
			glVertexAttribPointer(POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
			
			glEnableVertexAttribArray(NORMAL);
			glVertexAttribPointer(NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
			
			glEnableVertexAttribArray(TEX_COORD);
			glVertexAttribPointer(TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoord));

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->GetEBO());
			glBindVertexArray(0);
		}

		void OGLRenderer::SetStencilOptions(bool use, GLuint bit)
		{
			stencilTest = use;
			writeToStencil = true;
			stencilBit = bit;
		}

		void OGLRenderer::SetStencilOptions(bool use, GLuint bit, GLuint mask, GLuint compare, GLuint sfail, GLuint dfail, GLuint dpass)
		{
			stencilTest = use;
			writeToStencil = false;
			stencilBit = bit;
			stencilMask = mask;
			stencilCompareFn = compare;
			sFail = sfail;
			dFail = dfail;
			dPass = dpass;
		}

		void OGLRenderer::UploadVertices(const vector<Vec3>& vertices)
		{
			if (vertices.size() > 0)
			{
				verticesCount = vertices.size();
				glBindVertexArray(vertexArray);
				glGenBuffers(1, &vertexbuffers[POSITION]);
				glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[POSITION]);
				glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vec3), &vertices[0], GL_STATIC_DRAW);
				glVertexAttribPointer(POSITION, 3, GL_FLOAT, GL_FALSE, 0, 0);
				glEnableVertexAttribArray(POSITION);
				glBindVertexArray(0);
			}
		}

		void OGLRenderer::UploadNormals(const vector<Vec3>& normals)
		{
			if (normals.size() > 0)
			{
				glBindVertexArray(vertexArray);
				glGenBuffers(1, &vertexbuffers[NORMAL]);
				glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[NORMAL]);
				glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(Vec3), &normals[0], GL_STATIC_DRAW);
				glVertexAttribPointer(NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
				glEnableVertexAttribArray(NORMAL);
				glBindVertexArray(0);
			}
		}

		void OGLRenderer::UploadTexCoords(const vector<Vec2>& texCoords)
		{
			if (texCoords.size() > 0)
			{
				glBindVertexArray(vertexArray);
				glGenBuffers(1, &vertexbuffers[TEX_COORD]);
				glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[TEX_COORD]);
				glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(Vec2), &texCoords[0], GL_STATIC_DRAW);
				glVertexAttribPointer(TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, 0);
				glEnableVertexAttribArray(TEX_COORD);
				glBindVertexArray(0);
			}
		}

		void OGLRenderer::SetMVP(const Mat4& mvp)
		{
			glUseProgram(program);
			glUniformMatrix4fv(mvpLocation, 1, false, &mvp.elements[0]);
		}

		void OGLRenderer::SetTexture(const string uniform, const Texture2D& texture)
		{
			unordered_map<string, TexturePair>::iterator it = textures.find(uniform);
			if (it == textures.end())
			{
				TexturePair pair;
				pair.uniformLocation = glGetUniformLocation(program, uniform.c_str());
				pair.textureID = texture.GetTextureID();
				textures[uniform] = pair;
			}
			else
				it->second.textureID = texture.GetTextureID();
		}

		void OGLRenderer::SetUniform(const string uniform, const int& value)
		{
			unordered_map<string, GLint>::iterator it = uniformLocations.find(uniform);
			if (it == uniformLocations.end())
			{
				GLint location = glGetUniformLocation(program, uniform.c_str());
				uniformLocations[uniform] = location;
				glUniform1i(location, value);
			}
			else
				glUniform1i(it->second, value);
		}

		void OGLRenderer::SetUniform(const string uniform, const float& value)
		{
			unordered_map<string, GLint>::iterator it = uniformLocations.find(uniform);
			if (it == uniformLocations.end())
			{
				GLint location = glGetUniformLocation(program, uniform.c_str());
				uniformLocations[uniform] = location;
				glUniform1f(location, value);
			}
			else
				glUniform1f(it->second, value);
		}

		void OGLRenderer::SetUniform(const string uniform, const Vec2& value)
		{
			unordered_map<string, GLint>::iterator it = uniformLocations.find(uniform);
			if (it == uniformLocations.end())
			{
				GLint location = glGetUniformLocation(program, uniform.c_str());
				uniformLocations[uniform] = location;
				glUniform2f(location, value.x, value.y);
			}
			else
				glUniform2f(it->second, value.x, value.y);
		}

		void OGLRenderer::SetUniform(const string uniform, const Vec3& value)
		{
			unordered_map<string, GLint>::iterator it = uniformLocations.find(uniform);
			if (it == uniformLocations.end())
			{
				GLint location = glGetUniformLocation(program, uniform.c_str());
				uniformLocations[uniform] = location;
				glUniform3f(location, value.x, value.y, value.z);
			}
			else
				glUniform3f(it->second, value.x, value.y, value.z);
		}

		void OGLRenderer::SetUniform(const string uniform, const Vec4& value)
		{
			unordered_map<string, GLint>::iterator it = uniformLocations.find(uniform);
			if (it == uniformLocations.end())
			{
				GLint location = glGetUniformLocation(program, uniform.c_str());
				uniformLocations[uniform] = location;
				glUniform4f(location, value.x, value.y, value.z, value.w);
			}
			else
				glUniform4f(it->second, value.x, value.y, value.z, value.w);
		}

		void OGLRenderer::SetUniform(const string uniform, const Mat3& value)
		{
			unordered_map<string, GLint>::iterator it = uniformLocations.find(uniform);
			if (it == uniformLocations.end())
			{
				GLint location = glGetUniformLocation(program, uniform.c_str());
				uniformLocations[uniform] = location;
				glUniformMatrix3fv(location, 1, false, &value.elements[0]);
			}
			else
				glUniformMatrix3fv(it->second, 1, false, &value.elements[0]);
		}

		void OGLRenderer::SetUniform(const string uniform, const Mat4& value)
		{
			unordered_map<string, GLint>::iterator it = uniformLocations.find(uniform);
			if (it == uniformLocations.end())
			{
				GLint location = glGetUniformLocation(program, uniform.c_str());
				uniformLocations[uniform] = location;
				glUniformMatrix4fv(location, 1, false, &value.elements[0]);
			}
			else
				glUniformMatrix4fv(it->second, 1, false, &value.elements[0]);
		}
	}
}
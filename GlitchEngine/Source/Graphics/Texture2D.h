#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif

#define TEXTURES_DIR "Resources/Textures/"
#include <GL\glew.h>

#include <string>
#include <algorithm>

#define STBI_NO_PSD
#define STBI_NO_GIF
#define STBI_NO_PIC
#define STBI_NO_PNM
#include <Image/stb_image.h>
#include <Utilities\DDSLoader.h>

using namespace std;

namespace GlitchEngine
{
	namespace Graphics
	{
		class Texture2D
		{
		public:
			Texture2D() { };
			Texture2D(string& filepath, bool mipMaps = true);

			~Texture2D();
			void LoadTexture(string& filePath, bool mipMaps = true);

			GE_API void SetFilterMode(GLuint mode);
			GE_API void SetFilterMode(GLuint filter, GLuint mode);
			GE_API void SetWrapMode(GLuint mode);
			GE_API void SetWrapMode(GLuint axis, GLuint mode);
			
			GE_API int GetWidth() const;
			GE_API int GetHeight() const;
			GLuint GetTextureID() const;

		private:
			GLuint textureID;
			GLuint textureFormat;
			GLuint filterMode;
			GLuint wrapMode;

			unsigned char* imageData;
			int width;
			int height;
			int components;
			int mipMapCount = 0;
		};
	}
}
#pragma once
#define MODELS_DIR "Resources/Models/"

#include <vector>
#include <string>
#include <iostream>

#include <Assimp/importer.hpp>
#include <Assimp/postprocess.h>
#include <Assimp/scene.h>

#include <GL\glew.h>
#include <Math\GEMath.h>
#include <Graphics\Mesh.h>

using namespace std;
using namespace GlitchEngine::Math;

namespace GlitchEngine
{
	namespace Graphics
	{
		struct ModelNode
		{
			Mesh* parent;
			vector<Mesh*> children;
		};

		class Model
		{
		public:
			Model(string& filePath, bool flipUVs = false);
			~Model();

			void RecursiveMeshLookUp(aiNode* node, const aiScene* scene);
			Mesh* ProcessMesh(aiMesh* aiMesh, const aiScene *scene);

			vector<Mesh*> GetMeshTree() const { return meshes; }
		private:
			vector<Mesh*> meshes;
		};
	}
}
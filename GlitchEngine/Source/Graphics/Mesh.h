#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif

#include <vector>
#include <string>

#include <GL\glew.h>
#include <Math\GEMath.h>

using namespace std;
using namespace GlitchEngine::Math;

namespace GlitchEngine
{
	namespace Graphics
	{
		enum Buffers 
		{
			VERTEX_BUFFER,
			ELEMENT_BUFFER,
			BUFFER_COUNT
		};

		struct Vertex
		{
			Vec3 position;
			Vec3 normal;
			Vec2 texCoord;
			Vec3 tangent;
			Vec3 biTangent;
		};

		class Mesh
		{
		public:
			Mesh(vector<Vertex>& vertices, vector<GLuint>& indices);
			~Mesh();

			GLuint GetVBO() const;
			GLuint GetEBO() const;
			GLuint GetIndicesCount() const;

		private:
			GLuint bufferObjects[BUFFER_COUNT];
			GLuint indicesCount;
		};
	}
}
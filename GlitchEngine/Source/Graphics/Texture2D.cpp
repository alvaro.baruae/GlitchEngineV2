﻿#define DDS_LOADER_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include "Texture2D.h"

namespace GlitchEngine
{
	namespace Graphics
	{
		Texture2D::Texture2D(string& filePath, bool mipMaps)
		{
			LoadTexture(filePath, mipMaps);
		}

		Texture2D::~Texture2D()
		{
			glDeleteTextures(1, &textureID);
			delete imageData;
		}

		void Texture2D::LoadTexture(string& filePath, bool mipMaps)
		{
			if (filePath.find(".dds") == string::npos)
			{
				imageData = stbi_load(filePath.c_str(), &width, &height, &components, 0);
				textureFormat = components == 3 ? GL_RGB : GL_RGBA;
			}
			else
				imageData = LoadDDS(filePath.c_str(), &width, &height, &components, &textureFormat, &mipMapCount);

			if (textureFormat != DDS_FORMAT_UNSUPPORTED)
			{
				glGenTextures(1, &textureID);
				glBindTexture(GL_TEXTURE_2D, textureID);

				if (textureFormat >= GL_COMPRESSED_RGBA_S3TC_DXT1_EXT && textureFormat <= GL_COMPRESSED_RGBA_S3TC_DXT5_EXT)
				{
					int tWidth = width;
					int tHeight = height;
					unsigned int blockSize = (textureFormat == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
					
					unsigned int offset = 0;
					for (int level = 0; level < mipMapCount && (tWidth || tHeight); ++level)
					{
						unsigned int size = ((tWidth + 3) / 4) * ((tHeight + 3) / 4) * blockSize;
						glCompressedTexImage2D(GL_TEXTURE_2D, level, textureFormat, tWidth, tHeight, 0, size, imageData + offset);

						offset += size;
						tWidth /= 2;
						tHeight /= 2;
					}

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				}
				else
					glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, width, height, 0, textureFormat, GL_UNSIGNED_BYTE, imageData);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				if (mipMaps)
				{
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
					glGenerateMipmap(GL_TEXTURE_2D);
				}
				else
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			}
		}

		void Texture2D::SetFilterMode(GLuint mode)
		{
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mode);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mode);
		}

		void Texture2D::SetFilterMode(GLuint filter, GLuint mode)
		{
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, filter, mode);
		}

		void Texture2D::SetWrapMode(GLuint mode)
		{
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mode);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mode);
		}

		void Texture2D::SetWrapMode(GLuint axis, GLuint mode)
		{
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, axis, mode);
		}

		GLuint Texture2D::GetTextureID() const
		{
			return textureID;
		}

		int Texture2D::GetWidth() const
		{
			return width;
		}

		int Texture2D::GetHeight() const
		{
			return height;
		}
	}
}
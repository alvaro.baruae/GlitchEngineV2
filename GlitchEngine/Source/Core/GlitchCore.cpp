#include "GlitchCore.h"

namespace GlitchEngine
{
	GlitchCore::GlitchCore(UpdateFunction func)
	{
		function = func;
	}

	GlitchCore::~GlitchCore()
	{
		input->Destroy();
		time->Destroy();
		entityManager->Destroy();
		rendererManager->Destroy();
		resourcesManager->Destroy();
		shaderLoader->Destroy();
		window->Destroy();
	}

	void GlitchCore::Run()
	{
		while (!window->Closed())
		{
			if (window->IsVsyncEnabled())
			{
				time->SetTime((float)glfwGetTime());
				UpdateLoop();
			}
			else
			{
				float currentTime = (float)glfwGetTime();
				if (window->WaitForTargetFPS(currentTime))
				{
					time->SetTime(currentTime);
					UpdateLoop();
				}
			}
		}
	}

	bool GlitchCore::Initialize()
	{
		window = Window::GetInstance();
		time = Time::GetInstance();
		entityManager = EntityManager::GetInstance();
		shaderLoader = ShaderLoader::GetInstance();
		rendererManager = RendererManager::GetInstance();
		resourcesManager = ResourcesManager::GetInstance();

		if (!window->InitWindow("Glitch Engine v2", 960, 540))
			return false;

		input = Input::GetInstance();
		window->SetClearColor(0.15f, 0.15f, 0.15f);
		return true;
	}

	void GlitchCore::UpdateLoop()
	{
		window->Clear();
		entityManager->Update();
		rendererManager->Draw();
		function();
		window->Update();
	}
}
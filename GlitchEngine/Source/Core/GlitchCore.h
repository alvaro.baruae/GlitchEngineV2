#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif
#include <Entities\Entity.h>
#include <Entities\MeshEntity.h>
#include <Entities\CameraEntity.h>

#include <Components\Camera.h>
#include <Components\Transform.h>
#include <Components\Material.h>
#include <Components\MeshDefinition.h>
#include <Components\MeshRenderer.h>

#include <Managers\EntityManager.h>
#include <Managers\RendererManager.h>
#include <Managers\ResourcesManager.h>

#include <Utilities\Time.h>
#include <Utilities\Input.h>
#include <Utilities\ShaderLoader.h>

#include <Math\GEMath.h>

#include <Graphics\Window.h>
#include <Graphics\Texture2D.h>

namespace GlitchEngine
{
	using namespace Graphics;
	using namespace Managers;
	using namespace Entities;
	using namespace Utilities;
	using namespace Math;

	typedef void(*UpdateFunction)();
	
	class GE_API GlitchCore
	{
	public:
		GlitchCore(UpdateFunction func);
		~GlitchCore();

		bool Initialize();
		void Run();

	private:
		void UpdateLoop();

		UpdateFunction function;
		Input* input;
		Window* window;
		Time* time;
		EntityManager* entityManager;
		ShaderLoader* shaderLoader;
		RendererManager* rendererManager;
		ResourcesManager* resourcesManager;
	};
}


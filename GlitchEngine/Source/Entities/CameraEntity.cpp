#include "CameraEntity.h"

namespace GlitchEngine
{
	namespace Entities
	{
		CameraEntity::CameraEntity() : Entity()
		{
			InitComponents();
		}

		CameraEntity::CameraEntity(string name) : Entity(name)
		{
			InitComponents();
		}

		CameraEntity::~CameraEntity()
		{
		}

		void CameraEntity::InitComponents()
		{
			transform = AddComponent<Transform>();
			camera = AddComponent<Camera>();
		}
	}
}
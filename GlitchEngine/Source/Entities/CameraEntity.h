#pragma once
#include <Components\Transform.h>
#include <Components\Camera.h>

#include <Entities\Entity.h>

using namespace GlitchEngine::Components;

namespace GlitchEngine
{
	namespace Entities
	{
		class GE_API CameraEntity : public Entity
		{
		public:
			CameraEntity();
			CameraEntity(string name);

			~CameraEntity();

			CompPtr<Transform> transform;
			CompPtr<Camera> camera;
		
		protected:
			virtual void InitComponents() override;
		};
	}
}


#pragma once
#include <Components\Transform.h>
#include <Components\Material.h>
#include <Components\MeshDefinition.h>
#include <Components\MeshRenderer.h>

#include <Entities\Entity.h>

using namespace GlitchEngine::Components;
namespace GlitchEngine
{
	namespace Entities
	{
		class GE_API MeshEntity : public Entity
		{
		public:
			MeshEntity();
			MeshEntity(string name);
			~MeshEntity();

			CompPtr<Transform> transform;
			CompPtr<Material> material;
			CompPtr<MeshDefinition> meshDefinition;
			CompPtr<MeshRenderer> meshRenderer;
		
		protected:
			virtual void InitComponents() override;
		};
	}
}
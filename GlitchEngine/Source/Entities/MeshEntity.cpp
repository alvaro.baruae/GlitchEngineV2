#include "MeshEntity.h"

namespace GlitchEngine
{
	namespace Entities
	{
		MeshEntity::MeshEntity() : Entity()
		{
			InitComponents();
		}

		MeshEntity::MeshEntity(string name) : Entity(name)
		{
			InitComponents();
		}

		MeshEntity::~MeshEntity()
		{
		}

		void MeshEntity::InitComponents()
		{
			transform = AddComponent<Transform>();
			material = AddComponent<Material>();
			meshDefinition = AddComponent<MeshDefinition>();
			meshRenderer = AddComponent<MeshRenderer>();
		}
	}
}
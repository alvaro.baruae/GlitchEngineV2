#include "Entity.h"

namespace GlitchEngine
{
	namespace Entities
	{
		Entity::Entity()
		{
			PushToEntityManager();
		}

		Entity::Entity(string _name)
		{
			name = _name;
			PushToEntityManager();
		}

		Entity::~Entity()
		{
#ifdef _DEBUG
			for (unordered_map<unsigned short, CompBase*>::iterator it = dependants.begin(); it != dependants.end(); ++it)
				*it->second->isDead = true;
#endif
		}

		void Entity::Update() const
		{
			for (size_t i = 0; i < componentList.size(); ++i)
			{
				if (componentList[i]->GetActiveState())
					componentList[i]->Update();
			}
		}

		void Entity::SetActiveState(bool newActiveState)
		{
			isActive = newActiveState;
		}

		bool Entity::GetActiveState() const
		{
			return isActive;
		}

		void Entity::PushToEntityManager()
		{
			EntityManager::GetInstance()->AddEntity(this);
		}
	}
}
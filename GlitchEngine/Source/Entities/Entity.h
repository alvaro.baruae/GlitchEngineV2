#pragma once
#pragma warning(disable:4251)

#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif

#define GE_API_EXPORT __declspec(dllexport)

#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <unordered_map>

#include <Managers\EntityManager.h>
#include <Components\Component.h>

using namespace std;
using namespace GlitchEngine::Components;
using namespace GlitchEngine::Managers;

namespace GlitchEngine
{
	namespace Entities
	{
		class GE_API Entity
		{
		private:
			string name;
		
		public:
			Entity();
			Entity(string _name);
			~Entity();

			void Update() const;

			template<typename T>
			CompPtr<T> AddComponent()
			{
				T* c = new T();
				unsigned short compID = GetComponentID<T>();
				if (!addedComponents[compID])
				{
					Component* component = static_cast<Component*>(c);

					component->entity = this;
					componentReferences[compID] = (unsigned short)componentList.size();
					componentList.push_back(shared_ptr<Component>(component));
					addedComponents[compID] = true;
					component->Init();
#ifdef _DEBUG
					CompPtr<T>* comp = new CompPtr<T>(c);
					dependants[compID] = static_cast<CompBase*>(comp);
					return *comp;
#else
					return c;
#endif
				}
				else
				{
					cout << "A component of type '" << typeid(T).name() << "' already exists in entity '" << name << "', only one component of each type is allowed per entity." << endl;
					return CompPtr<T>();
				}
			}

			template<typename T>
			CompPtr<T>  GetComponent()
			{
				if (!HasComponent<T>()) {
					cout << "Component of type '" << typeid(T).name() << "' not present in entity '" << name << "'." << endl;
					return CompPtr<T>();
				}

				unsigned short compID = GetComponentID<T>();
				T* c = static_cast<T*>(componentList[componentReferences[compID]].get());
#ifdef _DEBUG
				unordered_map<unsigned short, CompBase*>::iterator it = dependants.find(compID);
				if (it == dependants.end())
				{
					CompPtr<T>* comp = new CompPtr<T>(c);
					dependants[compID] = static_cast<CompBase*>(comp);
					return *comp;
				}
				else
					return *static_cast<CompPtr<T>*>(it->second);
#else
				return c;
#endif
		}

			template<typename T>
			void RemoveComponent()
			{
				unsigned short compID = GetComponentID<T>();
				componentList[componentReferences[compID]]->Remove();
				componentList.erase(componentList.begin() + componentReferences[compID]);
#ifdef _DEBUG
				*dependants[compID]->isDead = true;
				delete dependants[compID];
#endif
				addedComponents[compID] = false;
			}

			template<typename T>
			bool HasComponent()
			{
				return addedComponents[GetComponentID<T>()];
			}

			template<typename T>
			void EnableComponent()
			{
				if (!HasComponent<T>()) {
					cout << "Component of type '" << typeid(T).name() << "' not present in entity '" << name << "'." << endl;
					return;
				}
				T* comp = GetComponent<T>();
				comp->SetActiveState(true);
				comp->Enable();
			}

			template<typename T>
			void DiableComponent()
			{
				if (!HasComponent<T>())
				{
					cout << "Component of type '" << typeid(T).name() << "' not present in entity '" << name << "'." << endl;
					return;
				}

				T* comp = GetComponent<T>();
				comp->SetActiveState(false);
				comp->Disable();
			}

			string GetName() const { return name; }
			void SetActiveState(bool newActiveState);
			bool GetActiveState() const;

		private:
			void PushToEntityManager();
			vector<shared_ptr<Component>> componentList;
			unordered_map<unsigned short, unsigned short> componentReferences;
			unordered_map<unsigned short, bool> addedComponents;

#ifdef _DEBUG
			unordered_map<unsigned short, CompBase*> dependants;
#endif
		protected:
			bool isActive = true;
			virtual void InitComponents() { };
	};
}
}
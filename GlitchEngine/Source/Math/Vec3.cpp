#include "GEMath.h"

namespace GlitchEngine
{
	namespace Math
	{
		Vec3 Vec3::AxisX = Vec3(1, 0, 0);
		Vec3 Vec3::AxisY = Vec3(0, 1, 0);
		Vec3 Vec3::AxisZ = Vec3(0, 0, 1);

		Vec3 Vec3::NAxisX = Vec3(-1, 0, 0);
		Vec3 Vec3::NAxisY = Vec3(0, -1, 0);
		Vec3 Vec3::NAxisZ = Vec3(0, 0, -1);

		Vec3::Vec3() 
		{
			this->x = 0.f;
			this->y = 0.f;
			this->z = 0.f;
		}

		Vec3::Vec3(const float& x, const float& y, const float& z)
		{
			this->x = x;
			this->y = y;
			this->z = z;
		}

		Vec3::Vec3(const Vec2& vec2)
		{
			this->x = vec2.x;
			this->y = vec2.y;
			this->z = 0.f;
		}

		Vec3::Vec3(const Vec4& vec4)
		{
			this->x = vec4.x;
			this->y = vec4.y;
			this->z = 0.f;
		}

		Vec3& Vec3::Add(const Vec3& other)
		{
			x += other.x;
			y += other.y;
			z += other.z;
			return *this;
		}

		Vec3& Vec3::Substract(const Vec3& other)
		{
			x -= other.x;
			y -= other.y;
			z -= other.z;
			return *this;
		}

		Vec3& Vec3::Divide(const Vec3& other)
		{
			x /= other.x;
			y /= other.y;
			z /= other.z;
			return *this;
		}

		Vec3& Vec3::Multiply(const Vec3& other)
		{
			x *= other.x;
			y *= other.y;
			z *= other.z;
			return *this;
		}

		Vec3& Vec3::Scale(const float factor)
		{
			x *= factor;
			y *= factor;
			z *= factor;
			return *this;
		}

		Vec3& Vec3::Rotate(const float angle, const Vec3 axis)
		{
			*this = Mat3::Rotation(angle, axis).Multiply(*this);
			return *this;
		}

		float Vec3::Dot(const Vec3& other)
		{
			return x * other.x + y * other.y + z * other.z;
		}

		float Vec3::Distance(Vec3 other)
		{
			other.Substract(*this);
			return sqrt(other.Dot(other));
		}

		float Vec3::Magnitude()
		{
			return sqrt(x * x + y * y + z * z);
		}

		Vec3& Vec3::Normalize()
		{
			float mag = Magnitude();
			if (mag != 0)
				this->Scale(1 / mag);
			return *this;
		}

		Vec3& Vec3::Cross(const Vec3& other)
		{
			x = y * other.z - z * other.y;
			y = z * other.x - x * other.z;
			z = x * other.y - y * other.x;
			return *this;
		}

		Vec3 operator+(Vec3 left, const Vec3& right)
		{
			return left.Add(right);
		}

		Vec3 operator-(Vec3 left, const Vec3& right)
		{
			return left.Substract(right);
		}

		Vec3 operator/(Vec3 left, const Vec3& right)
		{
			return left.Divide(right);
		}

		Vec3 operator*(Vec3 left, const Vec3& right)
		{
			return left.Multiply(right);
		}

		Vec3 operator*(Vec3 left, const float right)
		{
			return left.Scale(right);
		}

		Vec3& Vec3::operator=(const Vec2& vec2)
		{
			this->x = vec2.x;
			this->y = vec2.y;
			this->z = 0.f;
			return *this;
		}

		Vec3& Vec3::operator=(const Vec4& vec4)
		{
			this->x = vec4.x;
			this->y = vec4.y;
			this->z = vec4.z;
			return *this;
		}

		Vec3& Vec3::operator+=(const Vec3& other)
		{
			return Add(other);
		}

		Vec3& Vec3::operator-=(const Vec3& other)
		{
			return Substract(other);
		}

		Vec3& Vec3::operator/=(const Vec3& other)
		{
			return Divide(other);
		}

		Vec3& Vec3::operator*=(const Vec3& other)
		{
			return Multiply(other);
		}

		float& Vec3::operator[](const int& position)
		{
			return position == 0 ? x : position == 1 ? y : z;
		}

		bool Vec3::operator==(const Vec3& other)
		{
			return x == other.x && y == other.y && z == other.z;
		}

		bool Vec3::operator!=(const Vec3& other)
		{
			return !(*this == other);
		}

		ostream & operator<<(ostream& stream, const Vec3& vector)
		{
			stream << "Vec3(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
			return stream;
		}
	}
}
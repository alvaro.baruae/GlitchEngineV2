#pragma once

namespace GlitchEngine
{
	namespace Math
	{
		struct Mat4;
		struct GE_API Mat3
		{
			union 
			{
				float elements[9];
				Vec3 columns[3];
			};

			Mat3();
			Mat3(float diagonal);
			Mat3(const Mat4& mat4);

			Mat3& Multiply(const Mat3& other);
			Mat3& Transpose();
			Vec3 Multiply(const Vec3& other);
			
			static Mat3 Identity();
			static Mat3 Rotation(float angle, const Vec3& axis);
			
			GE_API friend Mat3 operator*(Mat3 left, const Mat3& right);
			GE_API friend Vec3 operator*(Mat3 left, const Vec3& right);

			Vec3& operator[] (const int& position);
			Mat3& operator*=(const Mat3& other);
			Vec3 GetRow(const int& position);
		};
	}
}


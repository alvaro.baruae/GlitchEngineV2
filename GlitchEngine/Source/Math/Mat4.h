#pragma once
namespace GlitchEngine
{
	namespace Math
	{
		struct GE_API Mat4
		{
			union
			{
				float elements[16];
				Vec4 columns[4];
			};

			Mat4();
			Mat4(float diagonal);
			Mat4& Multiply(const Mat4& other);
			Mat4& Rotate(float angle, Vec3& axis);
			Mat4& Inverse();
			Mat4& Transpose();
			Vec3 Multiply(const Vec3& other);
			Vec4 Multiply(const Vec4& other);

			static Mat4 Identity();
			static Mat4 Ortographic(float left, float right, float top, float bottom, float near, float far);
			static Mat4 Perspective(float fov, float aspect, float near, float far);

			static Mat4 Translation(const Vec3& translation);
			static Mat4 Rotation(float angle, const Vec3& axis);
			static Mat4 Scale(const Vec3& scale);

			GE_API friend Mat4 operator*(Mat4 left, const Mat4& right);
			GE_API friend Vec3 operator*(Mat4 left, const Vec3& right);
			GE_API friend Vec4 operator*(Mat4 left, const Vec4& right);

			Vec4& operator[] (const int& position);
			Mat4& operator*=(const Mat4& other);
			Vec4 GetRow(const int& position);
		};
	}
}


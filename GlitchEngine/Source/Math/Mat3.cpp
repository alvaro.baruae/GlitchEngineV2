#include "GEMath.h"

namespace GlitchEngine
{
	namespace Math
	{
		Mat3::Mat3()
		{
			memset(elements, 0, 9 * sizeof(float));
		}

		Mat3::Mat3(float diagonal)
		{
			memset(elements, 0, 9 * sizeof(float));
			elements[0] = diagonal;
			elements[4] = diagonal;
			elements[8] = diagonal;
		}

		Mat3::Mat3(const Mat4& mat4)
		{
			elements[0] = mat4.elements[0];
			elements[1] = mat4.elements[1];
			elements[2] = mat4.elements[2];

			elements[3] = mat4.elements[4];
			elements[4] = mat4.elements[5];
			elements[5] = mat4.elements[6];

			elements[6] = mat4.elements[8];
			elements[7] = mat4.elements[9];
			elements[8] = mat4.elements[10];
		}

		Mat3& Mat3::Multiply(const Mat3& other)
		{
			Mat3 result;
			for (int i = 0; i < 3; ++i)
			{
				Vec3 row = GetRow(i);
				for (int j = 0; j < 3; ++j)
				{
					result[j][i] = row.Dot(other.columns[j]);
				}
			}

			*this = result;
			return *this;
		}

		Mat3& Mat3::Transpose()
		{
			Mat3 res;
			for (int i = 0; i < 3; ++i)
				for (int j = 0; j < 3; ++j)
				{
					res[j][i] = columns[i][j];
				}
			*this = res;
			return *this;
		}

		Vec3 Mat3::Multiply(const Vec3& other)
		{
			Vec3 result;
			result.x = other.x * elements[0] + other.y * elements[1] + other.z * elements[2];
			result.y = other.x * elements[3] + other.y * elements[4] + other.z * elements[5];
			result.z = other.x * elements[6] + other.y * elements[7] + other.z * elements[8];
			return result;
		}

		Mat3 Mat3::Identity()
		{
			return Mat3(1.f);
		}

		Mat3 Mat3::Rotation(float angle, const Vec3& axis)
		{
			Mat3 result(1.f);
			float rad = angle * DEG2RAD;
			float c = cos(rad);
			float s = sin(rad);

			//Multiplying axis by 1-c to reduce further multiplications
			Vec3 omc = axis * (1 - c);

			result[0][0] = c + omc.x * axis.x;
			result[0][1] = omc.x * axis.y + s * axis.z;
			result[0][2] = omc.x * axis.z - s * axis.y;

			result[1][0] = omc.y * axis.x - s * axis.z;
			result[1][1] = c + omc.y * axis.y;
			result[1][2] = omc.y * axis.z + s * axis.x;

			result[2][0] = omc.z * axis.x + s * axis.y;
			result[2][1] = omc.z * axis.y - s * axis.x;
			result[2][2] = c + omc.z * axis.z;

			return result;
		}

		Mat3& Mat3::operator*=(const Mat3& other)
		{
			return Multiply(other);
		}

		Vec3 Mat3::GetRow(const int & position)
		{
			return Vec3(elements[position], elements[position + 3], elements[position + 6]);
		}

		Vec3& Mat3::operator[](const int& position)
		{
			return columns[position];
		}

		Mat3 operator*(Mat3 left, const Mat3& right)
		{
			return left.Multiply(right);
		}

		Vec3 operator*(Mat3 left, const Vec3 & right)
		{
			return left.Multiply(right);
		}
	}
}
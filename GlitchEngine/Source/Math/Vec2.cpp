#include "GEMath.h"

namespace GlitchEngine
{
	namespace Math
	{
		Vec2::Vec2() 
		{
			this->x = 0.f;
			this->y = 0.f;
		}

		Vec2::Vec2(const float& x, const float& y)
		{
			this->x = x;
			this->y = y;
		}

		Vec2::Vec2(const Vec3& vec3)
		{
			this->x = vec3.x;
			this->y = vec3.y;
		}

		Vec2::Vec2(const Vec4 & vec4)
		{
			this->x = vec4.x;
			this->y = vec4.y;
		}

		Vec2& Vec2::Add(const Vec2& other)
		{
			x += other.x;
			y += other.y;
			return *this;
		}

		Vec2& Vec2::Substract(const Vec2& other)
		{
			x -= other.x;
			y -= other.y;
			return *this;
		}

		Vec2& Vec2::Divide(const Vec2& other)
		{
			x /= other.x;
			y /= other.y;
			return *this;
		}

		Vec2& Vec2::Multiply(const Vec2& other)
		{
			x *= other.x;
			y *= other.y;
			return *this;
		}

		Vec2& Vec2::Scale(const float factor)
		{
			x *= factor;
			y *= factor;
			return *this;
		}

		Vec2& Vec2::Rotate(const float angle)
		{
			Vec2 result;

			float rad = angle * (float)M_PI / 180.f;
			const float c = cosf(rad);
			const float s = sinf(rad);

			result.x = x * c - y * s;
			result.y = x * s + y * c;

			x = result.x;
			y = result.y;
			return *this;
		}

		float Vec2::Dot(const Vec2& other)
		{
			return x * other.x + y * other.y;
		}

		float Vec2::Distance(Vec2 other)
		{
			other.Substract(*this);
			return sqrt(other.Dot(other));
		}

		float Vec2::Magnitude()
		{
			return sqrt(x * x + y * y);
		}

		Vec2& Vec2::Normalize()
		{
			float mag = Magnitude();
			if (mag != 0)
				this->Scale(1 / mag);
			return *this;
		}

		Vec2 operator+(Vec2 left, const Vec2& right)
		{
			return left.Add(right);
		}

		Vec2 operator-(Vec2 left, const Vec2& right)
		{
			return left.Substract(right);
		}

		Vec2 operator/(Vec2 left, const Vec2& right)
		{
			return left.Divide(right);
		}

		Vec2 operator*(Vec2 left, const Vec2& right)
		{
			return left.Multiply(right);
		}

		Vec2 operator*(Vec2 left, const float right)
		{
			return left.Scale(right);
		}

		Vec2& Vec2::operator=(const Vec3& vec3)
		{
			this->x = vec3.x;
			this->y = vec3.y;
			return *this;
		}

		Vec2& Vec2::operator=(const Vec4& vec4)
		{
			this->x = vec4.x;
			this->y = vec4.y;
			return *this;
		}

		Vec2& Vec2::operator+=(const Vec2& other)
		{
			return Add(other);
		}

		Vec2& Vec2::operator-=(const Vec2& other)
		{
			return Substract(other);
		}

		Vec2& Vec2::operator/=(const Vec2& other)
		{
			return Divide(other);
		}

		Vec2& Vec2::operator*=(const Vec2& other)
		{
			return Multiply(other);
		}

		bool Vec2::operator==(const Vec2& other)
		{
			return x == other.x && y == other.y;
		}

		bool Vec2::operator!=(const Vec2& other)
		{
			return !(*this == other);
		}

		float & Vec2::operator[](const int & position)
		{
			return position == 0 ? x : y;
		}

		ostream & operator<<(ostream& stream, const Vec2& vector)
		{
			stream << "Vec2(" << vector.x << ", " << vector.y << ")";
			return stream;
		}
	}
}
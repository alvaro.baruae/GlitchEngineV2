#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif

#define _USE_MATH_DEFINES
#define DEG2RAD 0.0174532925199432957692f
#define RAD2DEG 57.295779513082320876846f

#include <math.h>
#include <ostream>

#include <Math\Vec2.h>
#include <Math\Vec3.h>
#include <Math\Vec4.h>
#include <Math\Mat3.h>
#include <Math\Mat4.h>
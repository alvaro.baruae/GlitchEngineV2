#include "GEMath.h"

namespace GlitchEngine
{
	namespace Math
	{
		Vec4::Vec4() 
		{
			this->x = 0.f;
			this->y = 0.f;
			this->z = 0.f;
			this->w = 0.f;
		}

		Vec4::Vec4(const float& x, const float& y, const float& z, const float& w)
		{
			this->x = x;
			this->y = y;
			this->z = z;
			this->w = w;
		}

		Vec4::Vec4(const Vec2 & vec2)
		{
			this->x = vec2.x;
			this->y = vec2.y;
			this->z = 0.f;
			this->w = 0.f;
		}

		Vec4::Vec4(const Vec3 & vec3)
		{
			this->x = vec3.x;
			this->y = vec3.y;
			this->z = vec3.z;
			this->w = 0.f;
		}

		Vec4& Vec4::Add(const Vec4& other)
		{
			x += other.x;
			y += other.y;
			z += other.z;
			w += other.w;
			return *this;
		}

		Vec4& Vec4::Substract(const Vec4& other)
		{
			x -= other.x;
			y -= other.y;
			z -= other.z;
			w -= other.w;
			return *this;
		}

		Vec4& Vec4::Divide(const Vec4& other)
		{
			x /= other.x;
			y /= other.y;
			z /= other.z;
			w /= other.w;
			return *this;
		}

		Vec4& Vec4::Multiply(const Vec4& other)
		{
			x *= other.x;
			y *= other.y;
			z *= other.z;
			w *= other.w;
			return *this;
		}

		Vec4& Vec4::Scale(const float factor)
		{
			x *= factor;
			y *= factor;
			z *= factor;
			w *= factor;
			return *this;
		}

		Vec4& Vec4::Rotate(const float angle, const Vec3 axis)
		{
			*this = Mat4::Rotation(angle, axis).Multiply(*this);
			return *this;
		}

		float Vec4::Dot(const Vec4& other)
		{
			return (x * other.x) + (y * other.y) + (z * other.z) + (w * other.w);
		}

		float Vec4::Distance(Vec4 other)
		{
			other.Substract(*this);
			return sqrt(other.Dot(other));
		}

		float Vec4::Magnitude()
		{
			return sqrt(x * x + y * y + z * z + w * w);
		}

		Vec4& Vec4::Normalize()
		{
			float mag = Magnitude();
			if (mag != 0)
				this->Scale(1 / mag);
			return *this;
		}

		Vec4 operator+(Vec4 left, const Vec4& right)
		{
			return left.Add(right);
		}

		Vec4 operator-(Vec4 left, const Vec4& right)
		{
			return left.Substract(right);
		}

		Vec4 operator/(Vec4 left, const Vec4& right)
		{
			return left.Divide(right);
		}

		Vec4 operator*(Vec4 left, const Vec4& right)
		{
			return left.Multiply(right);
		}

		Vec4 operator*(Vec4 left, const float right)
		{
			return left.Scale(right);
		}

		Vec4 & Vec4::operator=(const Vec2& vec2)
		{
			this->x = vec2.x;
			this->y = vec2.y;
			this->z = 0.f;
			this->w = 0.f;
			return *this;
		}

		Vec4 & Vec4::operator=(const Vec3& vec3)
		{
			this->x = vec3.x;
			this->y = vec3.y;
			this->z = vec3.z;
			this->w = 0.f;
			return *this;
		}

		Vec4& Vec4::operator+=(const Vec4& other)
		{
			return Add(other);
		}

		Vec4& Vec4::operator-=(const Vec4& other)
		{
			return Substract(other);
		}

		Vec4& Vec4::operator/=(const Vec4& other)
		{
			return Divide(other);
		}

		Vec4& Vec4::operator*=(const Vec4& other)
		{
			return Multiply(other);
		}

		float& Vec4::operator[](const int& position)
		{
			return position == 0 ? x : position == 1 ? y : position == 2 ? z : w;
		}

		bool Vec4::operator==(const Vec4& other)
		{
			return x == other.x && y == other.y && z == other.z && w == other.w;
		}

		bool Vec4::operator!=(const Vec4& other)
		{
			return !(*this == other);
		}

		ostream & operator<<(ostream& stream, const Vec4& vector)
		{
			stream << "Vec4(" << vector.x << ", " << vector.y << ", " << vector.z << ", " << vector.w << ")";
			return stream;
		}
	}
}
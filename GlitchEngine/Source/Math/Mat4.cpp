#include "GEMath.h"

namespace GlitchEngine
{
	namespace Math
	{
		Mat4::Mat4()
		{
			memset(elements, 0, 16 * sizeof(float));
		}

		Mat4::Mat4(float diagonal)
		{
			memset(elements, 0, 16 * sizeof(float));
			elements[0] = diagonal;
			elements[5] = diagonal;
			elements[10] = diagonal;
			elements[15] = diagonal;
		}

		Mat4& Mat4::Multiply(const Mat4& other)
		{
			Mat4 result;
			for (int i = 0; i < 4; ++i)
			{
				Vec4 row = GetRow(i);
				for (int j = 0; j < 4; ++j)
				{
					result[j][i] = row.Dot(other.columns[j]);
				}
			}

			*this = result;
			return *this;
		}

		Mat4& Mat4::Transpose()
		{
			Mat4 res;
			for (int i = 0; i < 4; ++i)
				for (int j = 0; j < 4; ++j)
				{
					res[j][i] = columns[i][j];
				}
			*this = res;
			return *this;
		}

		Vec3 Mat4::Multiply(const Vec3& other)
		{
			Vec3 result;
			result.x = other.x * elements[0] + other.y * elements[1] + other.z * elements[2];
			result.y = other.x * elements[4] + other.y * elements[5] + other.z * elements[6];
			result.z = other.x * elements[8] + other.y * elements[9] + other.z * elements[10];
			return result;
		}

		Vec4 Mat4::Multiply(const Vec4& other)
		{
			Vec4 result;
			result.x = other.x * elements[0] + other.y * elements[1] + other.z * elements[2] + other.w * elements[3];
			result.y = other.x * elements[4] + other.y * elements[5] + other.z * elements[6] + other.w * elements[7];
			result.z = other.x * elements[8] + other.y * elements[9] + other.z * elements[10] + other.w * elements[11];
			result.w = other.x * elements[12] + other.y * elements[13] + other.z * elements[14] + other.w * elements[15];
			return result;
		}

		Mat4 & Mat4::Rotate(float angle, Vec3 & axis)
		{
			Mat4 templocalMatrix = Mat4::Rotation(angle, axis);

			Mat4 resultMatrix;
			resultMatrix[0] = columns[0] * templocalMatrix[0][0] + columns[1] * templocalMatrix[0][1] + columns[2] * templocalMatrix[0][2];
			resultMatrix[1] = columns[0] * templocalMatrix[1][0] + columns[1] * templocalMatrix[1][1] + columns[2] * templocalMatrix[1][2];
			resultMatrix[2] = columns[0] * templocalMatrix[2][0] + columns[1] * templocalMatrix[2][1] + columns[2] * templocalMatrix[2][2];
			resultMatrix[3] = columns[3];

			*this = resultMatrix;
			return *this;
		}

		Mat4 & Mat4::Inverse()
		{
			float inv[16];
			float det;

			inv[0] = elements[5] * elements[10] * elements[15] -
				elements[5] * elements[11] * elements[14] -
				elements[9] * elements[6] * elements[15] +
				elements[9] * elements[7] * elements[14] +
				elements[13] * elements[6] * elements[11] -
				elements[13] * elements[7] * elements[10];

			inv[4] = -elements[4] * elements[10] * elements[15] +
				elements[4] * elements[11] * elements[14] +
				elements[8] * elements[6] * elements[15] -
				elements[8] * elements[7] * elements[14] -
				elements[12] * elements[6] * elements[11] +
				elements[12] * elements[7] * elements[10];

			inv[8] = elements[4] * elements[9] * elements[15] -
				elements[4] * elements[11] * elements[13] -
				elements[8] * elements[5] * elements[15] +
				elements[8] * elements[7] * elements[13] +
				elements[12] * elements[5] * elements[11] -
				elements[12] * elements[7] * elements[9];

			inv[12] = -elements[4] * elements[9] * elements[14] +
				elements[4] * elements[10] * elements[13] +
				elements[8] * elements[5] * elements[14] -
				elements[8] * elements[6] * elements[13] -
				elements[12] * elements[5] * elements[10] +
				elements[12] * elements[6] * elements[9];

			inv[1] = -elements[1] * elements[10] * elements[15] +
				elements[1] * elements[11] * elements[14] +
				elements[9] * elements[2] * elements[15] -
				elements[9] * elements[3] * elements[14] -
				elements[13] * elements[2] * elements[11] +
				elements[13] * elements[3] * elements[10];

			inv[5] = elements[0] * elements[10] * elements[15] -
				elements[0] * elements[11] * elements[14] -
				elements[8] * elements[2] * elements[15] +
				elements[8] * elements[3] * elements[14] +
				elements[12] * elements[2] * elements[11] -
				elements[12] * elements[3] * elements[10];

			inv[9] = -elements[0] * elements[9] * elements[15] +
				elements[0] * elements[11] * elements[13] +
				elements[8] * elements[1] * elements[15] -
				elements[8] * elements[3] * elements[13] -
				elements[12] * elements[1] * elements[11] +
				elements[12] * elements[3] * elements[9];

			inv[13] = elements[0] * elements[9] * elements[14] -
				elements[0] * elements[10] * elements[13] -
				elements[8] * elements[1] * elements[14] +
				elements[8] * elements[2] * elements[13] +
				elements[12] * elements[1] * elements[10] -
				elements[12] * elements[2] * elements[9];

			inv[2] = elements[1] * elements[6] * elements[15] -
				elements[1] * elements[7] * elements[14] -
				elements[5] * elements[2] * elements[15] +
				elements[5] * elements[3] * elements[14] +
				elements[13] * elements[2] * elements[7] -
				elements[13] * elements[3] * elements[6];

			inv[6] = -elements[0] * elements[6] * elements[15] +
				elements[0] * elements[7] * elements[14] +
				elements[4] * elements[2] * elements[15] -
				elements[4] * elements[3] * elements[14] -
				elements[12] * elements[2] * elements[7] +
				elements[12] * elements[3] * elements[6];

			inv[10] = elements[0] * elements[5] * elements[15] -
				elements[0] * elements[7] * elements[13] -
				elements[4] * elements[1] * elements[15] +
				elements[4] * elements[3] * elements[13] +
				elements[12] * elements[1] * elements[7] -
				elements[12] * elements[3] * elements[5];

			inv[14] = -elements[0] * elements[5] * elements[14] +
				elements[0] * elements[6] * elements[13] +
				elements[4] * elements[1] * elements[14] -
				elements[4] * elements[2] * elements[13] -
				elements[12] * elements[1] * elements[6] +
				elements[12] * elements[2] * elements[5];

			inv[3] = -elements[1] * elements[6] * elements[11] +
				elements[1] * elements[7] * elements[10] +
				elements[5] * elements[2] * elements[11] -
				elements[5] * elements[3] * elements[10] -
				elements[9] * elements[2] * elements[7] +
				elements[9] * elements[3] * elements[6];

			inv[7] = elements[0] * elements[6] * elements[11] -
				elements[0] * elements[7] * elements[10] -
				elements[4] * elements[2] * elements[11] +
				elements[4] * elements[3] * elements[10] +
				elements[8] * elements[2] * elements[7] -
				elements[8] * elements[3] * elements[6];

			inv[11] = -elements[0] * elements[5] * elements[11] +
				elements[0] * elements[7] * elements[9] +
				elements[4] * elements[1] * elements[11] -
				elements[4] * elements[3] * elements[9] -
				elements[8] * elements[1] * elements[7] +
				elements[8] * elements[3] * elements[5];

			inv[15] = elements[0] * elements[5] * elements[10] -
				elements[0] * elements[6] * elements[9] -
				elements[4] * elements[1] * elements[10] +
				elements[4] * elements[2] * elements[9] +
				elements[8] * elements[1] * elements[6] -
				elements[8] * elements[2] * elements[5];

			det = elements[0] * inv[0] + elements[1] * inv[4] + elements[2] * inv[8] + elements[3] * inv[12];

			if (det == 0)
				return *this;

			det = 1.0f / det;

			Mat4 result;
			for (int i = 0; i < 16; ++i)
				result.elements[i] = inv[i] * det;
			*this = result;
			return *this;
		}

		Mat4 Mat4::Identity()
		{
			return Mat4(1.f);
		}

		Mat4 Mat4::Ortographic(float left, float right, float top, float bottom, float near, float far)
		{
			Mat4 result(1.f);

			result[0][0] = 2.0f / (right - left);
			result[1][1] = 2.0f / (top - bottom);
			result[2][2] = -2.0f / (far - near);

			result[3][0] = -(right + left) / (right - left);
			result[3][1] = -(top + bottom) / (top - bottom);
			result[3][2] = -(far + near) / (far - near);

			return result;
		}

		Mat4 Mat4::Perspective(float fov, float aspect, float near, float far)
		{
			Mat4 result;

			float rad = fov * DEG2RAD;
			float tanHFOV = tanf(rad / 2);
			float range = far - near;

			result[0][0] = 1 / (aspect * tanHFOV);
			result[1][1] = 1 / tanHFOV;
			result[2][2] = -(far + near) / range;
			result[2][3] = -1;
			result[3][2] = -(2 * far * near) / range;
			return result;
		}

		Mat4 Mat4::Translation(const Vec3 & translation)
		{
			Mat4 result(1.f);
			result[3][0] = translation.x;
			result[3][1] = translation.y;
			result[3][2] = translation.z;
			return result;
		}

		Mat4 Mat4::Rotation(float angle, const Vec3& axis)
		{
			Mat4 result(1.f);
			float rad = angle * DEG2RAD;
			float c = cosf(rad);
			float s = sinf(rad);

			//Multiplying axis by 1-c to reduce further multiplications
			Vec3 omc = axis * (1 - c);

			result[0][0] = c + omc.x * axis.x;
			result[0][1] = omc.x * axis.y + s * axis.z;
			result[0][2] = omc.x * axis.z - s * axis.y;

			result[1][0] = omc.y * axis.x - s * axis.z;
			result[1][1] = c + omc.y * axis.y;
			result[1][2] = omc.y * axis.z + s * axis.x;

			result[2][0] = omc.z * axis.x + s * axis.y;
			result[2][1] = omc.z * axis.y - s * axis.x;
			result[2][2] = c + omc.z * axis.z;

			return result;
		}

		Mat4 Mat4::Scale(const Vec3 & scale)
		{
			Mat4 result(1.f);
			result[0][0] = scale.x;
			result[1][1] = scale.y;
			result[2][2] = scale.z;
			return result;
		}

		Mat4& Mat4::operator*=(const Mat4& other)
		{
			return Multiply(other);
		}

		Vec4 Mat4::GetRow(const int& position)
		{
			return Vec4(elements[position], elements[position + 4], elements[position + 8], elements[position + 12]);
		}

		Vec4& Mat4::operator[](const int& position)
		{
			return columns[position];
		}

		Mat4 operator*(Mat4 left, const Mat4& right)
		{
			return left.Multiply(right);
		}

		Vec3 operator*(Mat4 left, const Vec3& right)
		{
			return left.Multiply(right);
		}

		Vec4 operator*(Mat4 left, const Vec4& right)
		{
			return left.Multiply(right);
		}
	}
}
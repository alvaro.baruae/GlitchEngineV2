#pragma once
using namespace std;

namespace GlitchEngine
{
	namespace Math
	{
		struct Vec2;
		struct Vec4;

		struct GE_API Vec3
		{
			float x, y, z;
			Vec3();
			Vec3(const float& x, const float& y, const float& z);
			Vec3(const Vec2& vec2);
			Vec3(const Vec4& vec4);

			static Vec3 AxisX;
			static Vec3 AxisY;
			static Vec3 AxisZ;

			static Vec3 NAxisX;
			static Vec3 NAxisY;
			static Vec3 NAxisZ;

			Vec3& Add(const Vec3& other);
			Vec3& Substract(const Vec3& other);
			Vec3& Divide(const Vec3& other);
			Vec3& Multiply(const Vec3& other);
			Vec3& Scale(const float factor);
			Vec3& Rotate(const float angle, const Vec3 axis);
			float Dot(const Vec3& other);
			float Distance(Vec3 other);
			float Magnitude();
			Vec3& Normalize();
			Vec3& Cross(const Vec3& other);

			GE_API friend Vec3 operator+(Vec3 left, const Vec3& right);
			GE_API friend Vec3 operator-(Vec3 left, const Vec3& right);
			GE_API friend Vec3 operator/(Vec3 left, const Vec3& right);
			GE_API friend Vec3 operator*(Vec3 left, const Vec3& right);
			GE_API friend Vec3 operator*(Vec3 left, const float right);
			
			Vec3& operator=(const Vec2& vec2);
			Vec3& operator=(const Vec4& vec4);
			Vec3& operator+=(const Vec3& other);
			Vec3& operator-=(const Vec3& other);
			Vec3& operator/=(const Vec3& other);
			Vec3& operator*=(const Vec3& other);
			float& operator[](const int& position);

			bool operator==(const Vec3& other);
			bool operator!=(const Vec3& other);

			GE_API friend ostream& operator<<(ostream& stream, const Vec3& vector);
		};
	}
}
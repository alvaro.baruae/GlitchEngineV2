#pragma once
using namespace std;

namespace GlitchEngine
{
	namespace Math
	{
		struct Vec3;
		struct Vec4;

		struct GE_API Vec2
		{
			float x, y;

			Vec2();
			Vec2(const float& x, const float& y);
			Vec2(const Vec3& vec3);
			Vec2(const Vec4& vec4);

			Vec2& Add(const Vec2& other);
			Vec2& Substract(const Vec2& other);
			Vec2& Divide(const Vec2& other);
			Vec2& Multiply(const Vec2& other);
			Vec2& Scale(const float factor);
			Vec2& Rotate(const float angle);
			float Dot(const Vec2& other);
			float Distance(Vec2 other);
			float Magnitude();
			Vec2& Normalize();

			GE_API friend Vec2 operator+(Vec2 left, const Vec2& right);
			GE_API friend Vec2 operator-(Vec2 left, const Vec2& right);
			GE_API friend Vec2 operator/(Vec2 left, const Vec2& right);
			GE_API friend Vec2 operator*(Vec2 left, const Vec2& right);
			GE_API friend Vec2 operator*(Vec2 left, const float right);

			Vec2& operator=(const Vec3& vec3);
			Vec2& operator=(const Vec4& vec4);
			Vec2& operator+=(const Vec2& other);
			Vec2& operator-=(const Vec2& other);
			Vec2& operator/=(const Vec2& other);
			Vec2& operator*=(const Vec2& other);

			bool operator==(const Vec2& other);
			bool operator!=(const Vec2& other);
			float& operator[](const int& position);

			GE_API friend ostream& operator<<(ostream& stream, const Vec2& vector);
		};
	}
}
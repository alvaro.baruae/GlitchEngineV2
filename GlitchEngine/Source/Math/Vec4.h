#pragma once
#include <cmath>

using namespace std;
namespace GlitchEngine
{
	namespace Math
	{
		struct Vec2;
		struct Vec3;

		struct GE_API Vec4
		{
			float x, y, z, w;

			Vec4();
			Vec4(const float& x, const float& y, const float& z, const float& w);
			Vec4(const Vec2& vec2);
			Vec4(const Vec3& vec3);

			Vec4& Add(const Vec4& other);
			Vec4& Substract(const Vec4& other);
			Vec4& Divide(const Vec4& other);
			Vec4& Multiply(const Vec4& other);
			Vec4& Scale(const float factor);
			Vec4& Rotate(const float angle, const Vec3 axis);
			float Dot(const Vec4& other);
			float Distance(Vec4 other);
			float Magnitude();
			Vec4& Normalize();

			GE_API friend Vec4 operator+(Vec4 left, const Vec4& right);
			GE_API friend Vec4 operator-(Vec4 left, const Vec4& right);
			GE_API friend Vec4 operator/(Vec4 left, const Vec4& right);
			GE_API friend Vec4 operator*(Vec4 left, const Vec4& right);
			GE_API friend Vec4 operator*(Vec4 left, const float right);

			Vec4& operator=(const Vec2& vec2);
			Vec4& operator=(const Vec3& vec3);
			Vec4& operator+=(const Vec4& other);
			Vec4& operator-=(const Vec4& other);
			Vec4& operator/=(const Vec4& other);
			Vec4& operator*=(const Vec4& other);
			float& operator[](const int& position);

			bool operator==(const Vec4& other);
			bool operator!=(const Vec4& other);

			GE_API friend ostream& operator<<(ostream& stream, const Vec4& vector);
		};
	}
}
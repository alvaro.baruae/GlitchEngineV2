#include "EntityManager.h"
#include <Entities/Entity.h>

namespace GlitchEngine
{
	using namespace Entities;

	namespace Managers
	{
		EntityManager* EntityManager::Instance = nullptr;

		EntityManager::EntityManager()
		{
		}

		EntityManager::~EntityManager()
		{
			for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
				delete (*it);
		}

		EntityManager* EntityManager::GetInstance()
		{
			if (!Instance)
				Instance = new EntityManager();
			return Instance;
		}

		void EntityManager::Destroy()
		{
			if (Instance != nullptr)
				delete Instance;
		}

		void EntityManager::AddEntity(Entity* entity)
		{
			entities.push_back(entity);
		}

		void EntityManager::RemoveEntity(Entity* entity)
		{
			vector<Entity*>::iterator it = std::find(entities.begin(), entities.end(), entity);
			if (it != entities.end())
				entities.erase(it);
		}

		void EntityManager::DestroyEntity(Entity** entity)
		{
			Instance->RemoveEntity((*entity));
			delete (*entity);
			(*entity) = nullptr;
		}

		void EntityManager::Update()
		{
			for (vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
				if ((*it)->GetActiveState())
					(*it)->Update();
		}
	}
}
#pragma once
#include <iostream>
#include <unordered_map>
#include <string>

#include <Graphics\Model.h>
#include <Graphics\Texture2D.h>

using namespace std;
using namespace GlitchEngine::Graphics;

namespace GlitchEngine
{
	namespace Managers
	{
		class ResourcesManager
		{
		public:
			ResourcesManager();
			~ResourcesManager();
			GE_API void LoadTexture(string key, string filePath, bool mipMaps = true);
			GE_API void LoadModel(string key, string filePath, bool flipUvs = false);
			GE_API Texture2D* GetTexture(string key);
			GE_API Model* GetModel(string key);
			
			GE_API static ResourcesManager* GetInstance();
			void Destroy();
		
		private:
			static ResourcesManager* Instance;
			unordered_map<string, Texture2D*> textures;
			unordered_map<string, Model*> models;
		};
	}
}
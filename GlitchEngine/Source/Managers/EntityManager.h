#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif

#include <vector>
using namespace std;
namespace GlitchEngine
{
	namespace Entities { class Entity; }
	using namespace Entities;

	namespace Managers
	{
		class EntityManager
		{
		public:
			EntityManager();
			~EntityManager();
			static EntityManager* GetInstance();

			void Destroy();
			void AddEntity(Entity* entity);
			void RemoveEntity(Entity* entity);
			GE_API static void DestroyEntity(Entity** entity);

			void Update();

		private:
			static EntityManager* Instance;
			vector<Entity*> entities;
		};
	}
}
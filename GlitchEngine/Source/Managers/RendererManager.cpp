#include "RendererManager.h"
#include <Components/MeshRenderer.h>

namespace GlitchEngine
{
	namespace Managers
	{
		RendererManager* RendererManager::Instance = nullptr;

		RendererManager::RendererManager()
		{
			window = Window::GetInstance();
		}


		RendererManager::~RendererManager()
		{
		}

		RendererManager* RendererManager::GetInstance()
		{
			if (!Instance)
				Instance = new RendererManager();
			return Instance;
		}

		void RendererManager::Destroy()
		{
			if (Instance)
				delete Instance;
		}

		void RendererManager::AddRenderer(MeshRenderer* renderer)
		{
			renderers.push_back(renderer);
		}

		void RendererManager::Draw() const
		{
			for (vector<MeshRenderer*>::const_iterator it = renderers.begin(); it != renderers.end(); ++it)
				(*it)->Draw();
		}

		void RendererManager::EnableScissorRegion(float xPercent, float yPercent, float widthPercent, float heightPercent)
		{
			glEnable(GL_SCISSOR_TEST);
			glScissor((GLuint)(xPercent * window->GetWidth()), (GLuint)(yPercent * window->GetHeight()), 
				(GLuint)(widthPercent * window->GetWidth()), (GLuint)(heightPercent * window->GetHeight()));
		}

		void RendererManager::DisableScissorRegion()
		{
			glDisable(GL_SCISSOR_TEST);
		}
	}
}
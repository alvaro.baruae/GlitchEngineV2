#include "ResourcesManager.h"

namespace GlitchEngine
{
	namespace Managers
	{
		ResourcesManager* ResourcesManager::Instance = nullptr;

		ResourcesManager::ResourcesManager()
		{
		}

		ResourcesManager::~ResourcesManager()
		{
			for (unordered_map<string, Texture2D*>::iterator it = textures.begin(); it != textures.end(); ++it)
				delete it->second;

			for (unordered_map<string, Model*>::iterator it = models.begin(); it != models.end(); ++it)
				delete it->second;
		}

		void ResourcesManager::LoadTexture(string key, string filePath, bool mipMaps)
		{
			textures[key] = new Texture2D(filePath, mipMaps);
		}

		void ResourcesManager::LoadModel(string key, string filePath, bool flipUvs)
		{
			models[key] = new Model(filePath, flipUvs);
		}

		Texture2D* ResourcesManager::GetTexture(string key)
		{
			unordered_map<string, Texture2D*>::iterator it = textures.find(key);
			if (it == textures.end())
				cout << "Texture of name '" << key << "' not found." << endl;
			return it->second;
		}

		Model* ResourcesManager::GetModel(string key)
		{
			unordered_map<string, Model*>::iterator it = models.find(key);
			if (it == models.end())
				cout << "Model of name '" << key << "' not found." << endl;
			return it->second;
		}

		ResourcesManager* ResourcesManager::GetInstance()
		{
			if (!Instance)
				Instance = new ResourcesManager();
			return Instance;
		}

		void ResourcesManager::Destroy()
		{
			if (Instance)
				delete Instance;
		}
	}
}
#pragma once
#ifdef EXPORT_GE_API
#define GE_API __declspec(dllexport)
#else
#define GE_API __declspec(dllimport)
#endif

#include <vector>
#include <Graphics\Window.h>

using namespace std;

namespace GlitchEngine { namespace Components { class MeshRenderer; } }
using namespace GlitchEngine::Components;
using namespace GlitchEngine::Graphics;

namespace GlitchEngine
{
	namespace Managers
	{
		class  RendererManager
		{
		public:
			RendererManager();
			~RendererManager();

			GE_API static RendererManager* GetInstance();
			void Destroy();
		
			void AddRenderer(MeshRenderer* renderer);
			void Draw() const;

			GE_API void EnableScissorRegion(float xPercent, float yPercent, float widthPercent, float heightPercent);
			GE_API void DisableScissorRegion();

		private:
			static RendererManager* Instance;
			Window* window;
			vector<MeshRenderer*> renderers;
		};
	}
}
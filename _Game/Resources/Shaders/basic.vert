#version 330 core

in vec3 position;
in vec3 normal;
in vec2 texCoord;

uniform mat4 mvp;

out VertexData
{
	vec2 uv;
	vec3 normal;
} OUT;

void main()
{
	OUT.uv = texCoord;
	OUT.normal = normal;
	gl_Position = mvp * vec4(position, 1.0f);
}
#version 330 core

in VertexData
{
	vec2 uv;
	vec3 normal;
} IN;

uniform sampler2D diffuseTex;
out vec4 fragColor;

void main()
{
	vec4 color = texture(diffuseTex, IN.uv).rgba;
	fragColor = color;
}
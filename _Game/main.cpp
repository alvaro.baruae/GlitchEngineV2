#include <Core\GlitchCore.h>

using namespace GlitchEngine;
using namespace GlitchEngine::Components;
using namespace GlitchEngine::Entities;

void Update();
MeshEntity* ent = new MeshEntity("HOLA");
MeshEntity* ent2 = new MeshEntity("HOLA2");

int main()
{
	GlitchCore core(Update);
	if(!core.Initialize())
		return 1;

	CameraEntity* mainCamera = new CameraEntity("Main Camera");
	mainCamera->camera->SetAsActiveCamera();

	ResourcesManager::GetInstance()->LoadTexture("brick", TEXTURES_DIR"brick.dds");
	ResourcesManager::GetInstance()->LoadModel("bb8", MODELS_DIR"bb8.obj");
	ResourcesManager::GetInstance()->LoadModel("cube", MODELS_DIR"cube.obj");
	ShaderLoader::GetInstance()->LoadShader("Basic", SHADERS_DIR"basic");

	ent->transform->SetPosition(Vec3(0, 0, -500));
	ent->material->SetShader("Basic");
	ent->meshDefinition->LoadMesh("bb8");
	ent->meshRenderer->InitRenderer();

	ent2->transform->SetPosition(Vec3(0, -150, -500));
	ent2->transform->SetScale(Vec3(0.5f, 0.5f, 0.5f));
	ent2->material->SetShader("Basic");
	ent2->meshDefinition->LoadMesh("bb8");
	ent2->meshRenderer->InitRenderer();
	
	core.Run();
	return 0;
}

void Update()
{
	if (Input::GetKey(GLFW_KEY_A))
		ent->transform->Translate(Vec3::NAxisX, WorldSpace);
	if (Input::GetKey(GLFW_KEY_D))
		ent->transform->Translate(Vec3::AxisX, WorldSpace);

	if (Input::GetKey(GLFW_KEY_W))
		ent->transform->Translate(Vec3::AxisY, WorldSpace);
	if (Input::GetKey(GLFW_KEY_S))
		ent->transform->Translate(Vec3::NAxisY, WorldSpace);

	if (Input::GetKey(GLFW_KEY_Q))
		ent->transform->Rotate(1, Vec3::AxisZ);
	if (Input::GetKey(GLFW_KEY_E))
		ent->transform->Rotate(1, Vec3::NAxisX);

	if (Input::GetKeyDown(GLFW_KEY_P))
		ent->transform->AddChild(ent2->transform);
	if (Input::GetKeyDown(GLFW_KEY_O))
		ent->transform->RemoveChild(0);

	cout << ent2->transform->GetLocalPosition() << endl;
}